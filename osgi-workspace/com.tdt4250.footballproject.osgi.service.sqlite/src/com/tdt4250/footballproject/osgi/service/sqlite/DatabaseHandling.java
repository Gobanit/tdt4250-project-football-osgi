package com.tdt4250.footballproject.osgi.service.sqlite;

import java.util.HashMap;
import java.util.Map;

import com.tdt4250.footballproject.model.football.FootballFactory;
import com.tdt4250.footballproject.model.football.League;
import com.tdt4250.footballproject.model.football.Match;
import com.tdt4250.footballproject.model.football.Team;
import com.tdt4250.footballproject.osgi.service.sqlite.model.SqlLeague;
import com.tdt4250.footballproject.osgi.service.sqlite.model.SqlMatch;
import com.tdt4250.footballproject.osgi.service.sqlite.model.SqlTeam;

public class DatabaseHandling {
	
	/*
	 * Create an instance for SqliteDB
	 * Uses HashMap to give each team their respectable id
	 */
	private final SqliteDB sqliteDB;
	private final Map<Integer, Team> idTeam = new HashMap<Integer, Team>();
	
	/*
	 * Instantiate sqliteDB
	 */
	public DatabaseHandling() {
		this.sqliteDB = new SqliteDB();
	}
	
	/*
	 * @return FootballFactory
	 */
	private FootballFactory getFootballFactory() {
		return FootballFactory.eINSTANCE; 
	}
	
	/*
	 * Add matches and teams to League object
	 * @param leagueID
	 * @return League object
	 */
	public League getLeague(String leagueID) {
		League league = getFootballFactory().createLeague();
		SqlLeague sqlLeague = this.sqliteDB.getValuesFromDB(leagueID);
		league.setCode("DBLEAGUE_"+sqlLeague.getLeagueId());
		league.setName(""+sqlLeague.getName());
		for (SqlTeam sqlTeam: sqlLeague.getTeams()) {
			Team team = getTeam(sqlTeam);
			league.getTeams().add(team);
		}
		for (SqlMatch sqlMatch: sqlLeague.getMatches()) {
			league.setCountry(sqlLeague.getName());
			Match match = getMatch(sqlMatch);
			league.getMatches().add(match);
			
		}
		return league;
	}
	
	/*
	 * Creates a Match object, sets different variables in match
	 * @param sqlMatch
	 * @return Match object
	 */
	public Match getMatch(SqlMatch sqlMatch) {
		Match match = getFootballFactory().createMatch();
		match.setDate(sqlMatch.getGameDate());
		match.setHomeTeam(idTeam.get(sqlMatch.getHid()));
		match.setHomeTeamScore(sqlMatch.getHgoal());
		match.setAwayTeam(idTeam.get(sqlMatch.getAid()));
		match.setAwayTeamScore(sqlMatch.getAgoal());
		return match;
	}
	
	/*
	 * Creates a Team object, sets team name, appends to HashMap
	 * @param sqlTeam
	 * @return Team object
	 */
	public Team getTeam(SqlTeam sqlTeam) {
		Team team = getFootballFactory().createTeam();
		team.setName(sqlTeam.getTeamName());
		idTeam.put(sqlTeam.getTeamId(), team);
		return team;
	}
	
}
