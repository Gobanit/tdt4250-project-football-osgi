package com.tdt4250.footballproject.osgi.service.sqlite;

import org.osgi.service.component.annotations.Component;

import com.tdt4250.footballproject.osgi.api.FootballService;

@Component(service = FootballService.class)
public class PremierLeagueService extends SqliteDataSourceService{
	
	/*
	 * League code for the league in England
	 */
	private final static String leagueID = "1729";
	
	/*
	 * Pass league code to superclass
	 */
	public PremierLeagueService() {
		super(leagueID);
		// TODO Auto-generated constructor stub
	}
	
}
