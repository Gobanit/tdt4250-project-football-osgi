package com.tdt4250.footballproject.osgi.service.sqlite;

import java.util.Date;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.tdt4250.footballproject.model.football.League;
import com.tdt4250.footballproject.model.football.Match;
import com.tdt4250.footballproject.osgi.api.FootballService;

/*
 * Sqlite database service
 */
public class SqliteDataSourceService implements FootballService {
	
	/*
	 * Create instance of League
	 * Create variable with league id
	 * Create instance of DatabaseHandling
	 */
	
	private League league;
	private final String leagueID;
	private final DatabaseHandling db;
	
	/*
	 * Constructor, sets leagueID, instantiates DatabaseHandling
	 * @param leagueID
	 */
	public SqliteDataSourceService(String leagueID) {
		this.leagueID = leagueID;
		db = new DatabaseHandling();
	}
	
	/*
	 * Fetches league object from DatabaseHandling's getLeague-function
	 * @return league object
	 */
	@Override
	public League getLeagueInfo() {
		League l = db.getLeague(leagueID);
		return l;
	}
	
	/*
	 * Fetches matches between two given dates
	 * @param from
	 * @param to
	 * @return list containing Match objects between two given dates
	 */

	@Override
	public List<Match> getMatches(Date from, Date to) {
		league = db.getLeague(leagueID);
		List<Match> matches = new ArrayList<Match>();
		for (Match match : league.getMatches()) {
			if (! (match.getDate().before(from) || match.getDate().after(to)))
				matches.add(match);
		}
		Collections.sort(matches, (m1, m2) -> m1.getDate().compareTo(m2.getDate()));
		return matches;
	}

}
