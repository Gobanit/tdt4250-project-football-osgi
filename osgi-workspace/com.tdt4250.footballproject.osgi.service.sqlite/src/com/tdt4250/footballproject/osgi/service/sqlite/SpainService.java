package com.tdt4250.footballproject.osgi.service.sqlite;

import org.osgi.service.component.annotations.Component;

import com.tdt4250.footballproject.osgi.api.FootballService;

@Component(service = FootballService.class)
public class SpainService extends SqliteDataSourceService{
	
	/*
	 * League code for the league in Spain
	 */
	private final static String leagueID = "21518";
	
	/*
	 * Pass league code to superclass
	 */
	public SpainService() {
		super(leagueID);
		// TODO Auto-generated constructor stub
	}

}
