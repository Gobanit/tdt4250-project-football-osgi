package com.tdt4250.footballproject.osgi.service.sqlite;

import org.osgi.service.component.annotations.Component;

import com.tdt4250.footballproject.osgi.api.FootballService;

@Component(service = FootballService.class)
public class FranceLeagueService extends SqliteDataSourceService{
	
	/*
	 * League code for the league in France	
	 */
	private final static String leagueID = "4769";
	
	/*
	 * Pass league code to superclass
	 */
	public FranceLeagueService() {
		super(leagueID);
		// TODO Auto-generated constructor stub
	}
	
	
}
