package com.tdt4250.footballproject.osgi.service.sqlite.model;

import java.util.Date;

public class SqlMatch {
	
	/*
	 * Created different variables for Match
	 */
	
	private int hid;
	private int aid;
	private String hteam;
	private String ateam;
	private Date gameDate;
	private int hgoal;
	private int agoal;

	public int getHid() {
		return hid;
	}

	public void setHid(int hid) {
		this.hid = hid;
	}

	public int getAid() {
		return aid;
	}

	public void setAid(int aid) {
		this.aid = aid;
	}
	
	
	public String getHteam() {
		return hteam;
	}
	public void setHteam(String hteam) {
		this.hteam = hteam;
	}
	public String getAteam() {
		return ateam;
	}
	public void setAteam(String ateam) {
		this.ateam = ateam;
	}
	public Date getGameDate() {
		return gameDate;
	}
	public void setGameDate(Date gameDate) {
		this.gameDate = gameDate;
	}
	public int getHgoal() {
		return hgoal;
	}
	public void setHgoal(int hgoal) {
		this.hgoal = hgoal;
	}
	public int getAgoal() {
		return agoal;
	}
	public void setAgoal(int agoal) {
		this.agoal = agoal;
	}
		

}
