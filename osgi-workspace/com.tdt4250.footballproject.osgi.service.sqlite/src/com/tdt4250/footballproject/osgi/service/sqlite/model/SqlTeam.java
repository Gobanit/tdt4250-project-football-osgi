package com.tdt4250.footballproject.osgi.service.sqlite.model;

public class SqlTeam {
	
	/*
	 * Create different variables for SqlTeam
	 */
	private int teamId;
	private String teamName;
	private String teamShortName;
	
	public int getTeamId() {
		return teamId;
	}
	public void setTeamId(int teamId) {
		this.teamId = teamId;
	}
	public String getTeamName() {
		return teamName;
	}
	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
	public String getTeamShortName() {
		return teamShortName;
	}
	public void setTeamShortName(String teamShortName) {
		this.teamShortName = teamShortName;
	}
	
	

}
