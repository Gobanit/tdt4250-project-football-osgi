package com.tdt4250.footballproject.osgi.service.sqlite.model;

import java.util.List;

public class SqlLeague {
	
	/* Create an id for league
	 * Create name for the league
	 * Create a list of matches
	 * Create a list of teams
	 */ 
	
	private int leagueId;
	private String name;
	private List<SqlMatch> matches;
	private List<SqlTeam> teams;
	
	public int getLeagueId() {
		return leagueId;
	}
	
	public void setLeagueId(int leagueId) {
		this.leagueId = leagueId;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public List<SqlMatch> getMatches() {
		return matches;
	}
	public void setMatches(List<SqlMatch> matches) {
		this.matches = matches;
	}
	
	public List<SqlTeam> getTeams() {
		return teams;
	}
	public void setTeams(List<SqlTeam> teams) {
		this.teams = teams;
	}


	
	
	
	

}
