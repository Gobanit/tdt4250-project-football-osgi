package com.tdt4250.footballproject.osgi.service.sqlite.activator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;

/**
 * Class serves to make the copy of sqlfile in current directory 
 * before starting the module.
 *
 */
public class Activator implements BundleActivator {

	private static final String DB_ORIG_RESOURCE_PATH = "data/database.sqlite";
	private static final String DB_TMP_FILE_PATH = "./temp_data.sqlite";
	
	public static String getDbTmpFilePath() {
		return DB_TMP_FILE_PATH;
	}
	
	@Override
	public void start(BundleContext context) throws Exception {
		System.out.println("Starting SQLITE bundle!!!");
		copyDBFile();
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		System.out.println("Stopping SQLITE bundle!!!");
		deleteDBFile();
	}
	
	private File copyDBFile() throws FileNotFoundException, IOException {
		File f = new File(DB_TMP_FILE_PATH);
		
		Bundle b = FrameworkUtil.getBundle(this.getClass());
		URL url = b.getResource(DB_ORIG_RESOURCE_PATH);
		
		InputStream is = url.openStream();
		System.out.println("Copying sqlite file...");
		byte[] bytes = IOUtils.toByteArray(is);
		is.close();
		
		FileOutputStream fos = new FileOutputStream(f);
		IOUtils.write(bytes, fos);
		fos.close();
		
		System.out.println("Created file "+f.getAbsolutePath());

		return f;
	}
	
	private void deleteDBFile() throws IOException {
		File f = new File(getDbTmpFilePath());
	
		FileUtils.forceDelete(f);

		System.out.println("File "+f.getAbsolutePath()+"deleted");
		
	}

}
