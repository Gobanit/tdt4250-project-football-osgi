package com.tdt4250.footballproject.osgi.service.sqlite;

import java.io.File;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import com.tdt4250.footballproject.osgi.service.sqlite.activator.Activator;
import com.tdt4250.footballproject.osgi.service.sqlite.model.SqlLeague;
import com.tdt4250.footballproject.osgi.service.sqlite.model.SqlMatch;
import com.tdt4250.footballproject.osgi.service.sqlite.model.SqlTeam;

public class SqliteDB {
	
	/* Create an instance of SqlLeague
	 * Create a list containing SqlMatch
	 * Create a list containing SqlTeam
	 */
	
	private SqlLeague sqlLeague;
	private List<SqlMatch> matches;
	private List<SqlTeam> teams;

	
	/*Fetches values from DB, sets values to each instance
	 * Appends matches and teams to SqlLeague
	 * @param leagueID
	 * @return sqlLeague object
	 */
	public SqlLeague getValuesFromDB(String leagueID) {
		sqlLeague = new SqlLeague();
		matches = new ArrayList<SqlMatch>();
		teams = new ArrayList<SqlTeam>();
		Connection c = null;
		Statement stmt = null;
		try {
			File f = new File(Activator.getDbTmpFilePath());

			Class.forName("org.sqlite.JDBC");
			String connString = "jdbc:sqlite:"+f.getAbsolutePath();
			c = DriverManager.getConnection(connString);

			c.setAutoCommit(false);
			stmt = c.createStatement();
			String query = "SELECT l.id lid, l.name lname, m.id mid, home.id hid, m.date mdate, "
					+ "away.id aid, home.team_long_name homename, home.team_short_name homeshort, "
					+ "away.team_short_name awayshort, "
					+ "away.team_long_name awayname, m.home_team_goal homegoal, m.away_team_goal awaygoal "
					+ "FROM League l LEFT OUTER JOIN Match m ON l.id=m.league_id "
					+ "LEFT OUTER JOIN Team home ON m.home_team_api_id=home.team_api_id LEFT OUTER JOIN "
					+ "Team away ON m.away_team_api_id=away.team_api_id WHERE l.id=" + leagueID + " GROUP BY mdate";
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				// TODO league data should be ideally fetched only once 
				sqlLeague.setLeagueId(rs.getInt("lid"));
				sqlLeague.setName(rs.getString("lname"));
								
				SqlMatch sqlMatch = new SqlMatch();
				SqlTeam sqlTeamHome = new SqlTeam();
				SqlTeam sqlTeamAway = new SqlTeam();
				sqlMatch.setHid(rs.getInt("hid"));
				sqlMatch.setAid(rs.getInt("aid"));
				sqlMatch.setAgoal(rs.getInt("awaygoal"));
				sqlMatch.setHgoal(rs.getInt("homegoal"));
				sqlMatch.setAteam(rs.getString("awayname"));
				sqlMatch.setHteam(rs.getString("homename"));
				sqlMatch.setGameDate(Date.valueOf(LocalDate.parse(rs.getString("mdate").substring(0, 10), DateTimeFormatter.ofPattern("yyyy-MM-dd"))));
				sqlTeamHome.setTeamId(rs.getInt("hid"));
				sqlTeamHome.setTeamName(rs.getString("homename"));
				sqlTeamHome.setTeamShortName(rs.getString("homeshort"));
				sqlTeamAway.setTeamId(rs.getInt("aid"));
				sqlTeamAway.setTeamName(rs.getString("homename"));
				sqlTeamAway.setTeamShortName(rs.getString("homeshort"));
				matches.add(sqlMatch);
				teams.add(sqlTeamHome);
				teams.add(sqlTeamAway);
			}
			sqlLeague.setMatches(matches);
			sqlLeague.setTeams(teams);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				c.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return sqlLeague;
	}
}
