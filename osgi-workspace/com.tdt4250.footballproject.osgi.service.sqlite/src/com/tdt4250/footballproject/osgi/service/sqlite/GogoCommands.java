package com.tdt4250.footballproject.osgi.service.sqlite;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.component.annotations.Component;

import com.tdt4250.footballproject.model.football.Match;
import com.tdt4250.footballproject.osgi.api.FootballService;

@Component(service = GogoCommands.class,
			property = {"osgi.command.scope=sqliteConsole",
						"osgi.command.function=matches"
					})
public class GogoCommands {
	
	
	public void matches(String leagueID) throws ParseException {
		try {
			matches(leagueID, "2008-08-16", "2008-10-25");
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void matches(String leagueID, String from, String to) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date dateFrom = format.parse(from);
		Date dateTo = format.parse(to);
		matches(leagueID, dateFrom, dateTo);
	}
	
	/*
	 * 
	 */
	public void matches(String leagueId, Date from, Date to) {
		FootballService fservice = null;
		BundleContext bc = FrameworkUtil.getBundle(this.getClass()).getBundleContext();
		try {
			for (ServiceReference<FootballService> serviceReference : bc.getServiceReferences(FootballService.class, null)) {
				FootballService service = bc.getService(serviceReference);
				if (leagueId.equals(service.getLeagueInfo().getCode())) {
					fservice = service;
					break;
				}
				
			}
			if (fservice != null) {
				System.out.println("Matches:");
				for (Match match : fservice.getMatches(from, to)) {
					System.out.println(getMatches(match));
				}
			} else {
				System.err.println("The league is not valid");
			}
		} catch (InvalidSyntaxException e) {
			System.err.println("Wrong Syntax when calling matches");
		}
		System.out.println();
		
	}
	
	private String getMatches(Match match) {
		return "Matches: [" + "Home team: " + match.getHomeTeam().getName() + " " + match.getHomeTeamScore() + " - "
				+ match.getAwayTeamScore() + " " + match.getAwayTeam().getName() + ": Away team :" + " Date: " + match.getDate()+ "]";
	}


}
