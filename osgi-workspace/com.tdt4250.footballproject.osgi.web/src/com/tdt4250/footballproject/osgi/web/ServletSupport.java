package com.tdt4250.footballproject.osgi.web;

import java.util.Date;
import java.util.List;

import com.tdt4250.footballproject.model.football.League;
import com.tdt4250.footballproject.model.football.Match;
import com.tdt4250.footballproject.osgi.api.FootballService;

public interface ServletSupport {

	
	public void addService(FootballService serv);
	
	public void removeService(FootballService serv);

	
	public List<Match> getLeagueMatches(Date fromDate, Date toDate, String leagueName);
	public List<League> getLeaguesInfo();
}
