package com.tdt4250.footballproject.osgi.web;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.eclipse.emf.common.util.EList;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.jaxrs.whiteboard.propertytypes.JaxrsResource;

import com.tdt4250.footballproject.model.football.League;
import com.tdt4250.footballproject.model.football.Match;
import com.tdt4250.footballproject.model.football.Team;
import com.tdt4250.footballproject.model.footballProvider.FootballProviderFactory;
import com.tdt4250.footballproject.model.footballProvider.LeagueChooser;
import com.tdt4250.footballproject.model.footballProvider.MatchesListResult;
import com.tdt4250.footballproject.model.html.util.FootballTransformations;

@Component(service = MatchController.class)
@JaxrsResource
public class MatchController {

	@Reference(cardinality = ReferenceCardinality.MANDATORY)
	private ServletSupport servSupport;
	

	@GET
	@Path("matches")
	@Produces({MediaType.TEXT_HTML}) 
	public String getMatches() {
		List<League> leagues = servSupport.getLeaguesInfo();
		LeagueChooser lc = wrapLeaguesList(leagues);
		String html = FootballTransformations.leaguesChooserToHtml(lc);
		return html;
	}
	
	@GET
	@Path("matches/search")
	@Produces({MediaType.TEXT_HTML})
	public String searchGet(@QueryParam("from_date") String fromDate, @QueryParam("to_date") String toDate, @QueryParam("league") String leagueCode) {
		if (fromDate.equals("") || (fromDate == null)) { fromDate = "1950-01-01"; }
		if (toDate.equals("") || (toDate == null)) { toDate = "2050-01-01"; }
		return searchPost(fromDate, toDate, leagueCode);
	}
	
	@POST
	@Path("matches/search")
	@Produces({MediaType.TEXT_HTML})
	public String searchPost(@FormParam("from_date") String fromDate, @FormParam("to_date") String toDate, @FormParam("league") String leagueCode) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		List<Match> matchList = null;
		
		if (fromDate.equals("") || (fromDate == null)) { fromDate = "1950-01-01"; }
		if (toDate.equals("") || (toDate == null)) { toDate = "2050-01-01"; }
		
		System.out.println(servSupport.toString());
		try {
			matchList = servSupport.getLeagueMatches(sdf.parse(fromDate), 
					sdf.parse(toDate), leagueCode);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		MatchesListResult mlr = wrapMatchesList(matchList);
		
		String html = FootballTransformations.matchesResultsToHTML(mlr);
		return html;
	}
	
	private MatchesListResult wrapMatchesList(List<Match> matches) {
		MatchesListResult mlr = FootballProviderFactory.eINSTANCE
				.createMatchesListResult();
		
		Set<League> leagues = new HashSet<>();
		for(Match m : matches) {
			leagues.add(m.getLeague());
			mlr.getFilteredMatches().add(m);
		}
		
		for(League l : leagues) {
			for(Match m : matches) {
				Team home = m.getHomeTeam();
				Team away = m.getAwayTeam();
				EList<Team> teams = l.getTeams();
				
				if(home != null && !teams.contains(home)) teams.add(home);
				if(away != null && !teams.contains(away)) teams.add(away);
			}
		}
		
		mlr.getLeagues().addAll(leagues);
		return mlr;
	}
	
	private LeagueChooser wrapLeaguesList(List<League> leagues) {
		LeagueChooser lc = FootballProviderFactory.eINSTANCE.createLeagueChooser();
		lc.getLeagues().addAll(leagues);
		return lc;
	}
}