package com.tdt4250.footballproject.osgi.web;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

import com.tdt4250.footballproject.model.football.FootballFactory;
import com.tdt4250.footballproject.model.football.League;
import com.tdt4250.footballproject.model.football.Match;
import com.tdt4250.footballproject.model.football.impl.FootballFactoryImpl;
import com.tdt4250.footballproject.osgi.api.FootballService;

@Component
public class ServletSupportImpl implements ServletSupport {

	List<FootballService> serviceList = new ArrayList<>();

	@Override
	public List<Match> getLeagueMatches(Date fromDate, Date toDate, String leagueCode) {
		
		List<Match> cumulativeList = new ArrayList<Match>();
		
		if (!this.serviceList.isEmpty()) {
			for (int index = 0; index < this.serviceList.size(); index++) {
				if (this.serviceList.get(index).getLeagueInfo().getCode().equals(leagueCode)) {
					return this.serviceList.get(index).getMatches(fromDate, toDate);	
				}
				cumulativeList.addAll(this.serviceList.get(index).getMatches(fromDate, toDate));
			}
			//If serviceList is non-empty, but no league-match: return matches from all leagues
			return cumulativeList;
		}
		return null;
	}

	@Reference(cardinality = ReferenceCardinality.MULTIPLE,
			policy = ReferencePolicy.DYNAMIC,
			bind = "addService",
			unbind = "removeService")
	@Override
	public void addService(FootballService serv) {
		System.out.println("Service added");
		this.serviceList.add(serv);
	}

	@Override
	public void removeService(FootballService serv) {
		if (this.serviceList.contains(serv)) {
			this.serviceList.remove(serv);
		}
	}

	@Override
	public List<League> getLeaguesInfo() {
		FootballFactory fact = FootballFactoryImpl.eINSTANCE;
		
		List<League> leagues = new ArrayList<>();
		for(FootballService serv : serviceList) {
			League l = fact.createLeague();
			l.setName(serv.getLeagueInfo().getName());
			l.setCode(serv.getLeagueInfo().getCode());
			leagues.add(l);
		}
		
		return leagues;
	}

}
