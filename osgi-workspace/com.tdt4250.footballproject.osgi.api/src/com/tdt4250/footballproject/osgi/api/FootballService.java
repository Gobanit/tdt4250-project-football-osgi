package com.tdt4250.footballproject.osgi.api;

import java.util.Date;
import java.util.List;

import org.osgi.annotation.versioning.ProviderType;

import com.tdt4250.footballproject.model.football.League;
import com.tdt4250.footballproject.model.football.Match;

/**
 * Interface for handling data about a football league
 *
 */
@ProviderType
public interface FootballService {
	
	/**
	 * Provides league object with its info, 
	 * but it does not necessarily contain relation
	 * @return
	 */
	public League getLeagueInfo();
	
	/**
	 * Method providing matches between specified dates.
	 * @param from - soonnest possible match date that would be contained in result
	 * @param to - latest possible match date that would be contained in result
	 * @return list of matchess
	 */
	public List<Match> getMatches(Date from, Date to);

}
