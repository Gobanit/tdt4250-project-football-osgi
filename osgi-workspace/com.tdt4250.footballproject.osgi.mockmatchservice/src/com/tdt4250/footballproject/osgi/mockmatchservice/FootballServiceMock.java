package com.tdt4250.footballproject.osgi.mockmatchservice;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.osgi.service.component.annotations.Component;

import com.tdt4250.footballproject.model.football.FootballFactory;
import com.tdt4250.footballproject.model.football.League;
import com.tdt4250.footballproject.model.football.Match;
import com.tdt4250.footballproject.model.football.Team;
import com.tdt4250.footballproject.model.football.impl.FootballFactoryImpl;
import com.tdt4250.footballproject.osgi.api.FootballService;

@Component
public class FootballServiceMock implements FootballService {
	
	@Override
	public List<Match> getMatches(Date from, Date to) {
		List<Match> list = new ArrayList<>();
		FootballFactory fact = FootballFactoryImpl.eINSTANCE;
					
		League league = createLeague();
		
		Team inter = fact.createTeam();
		inter.setName("Inter Bratislava");
		
		Team slovan = fact.createTeam();
		slovan.setName("Slovan Bratislava");
		
		Team bayern = fact.createTeam();
		bayern.setName("Bayern München");
		
		Team barcelona = fact.createTeam();
		barcelona.setName("FC Barcelona");
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm").withZone(ZoneId.systemDefault());
		
		Match match = fact.createMatch();
		match.setHomeTeam(inter);
		match.setAwayTeam(slovan);
		match.setDate(Date.from(Instant.from(dtf.parse("2019-12-24 18:00"))));
		match.setLeague(league);
		
		Match match2 = fact.createMatch();
		match2.setHomeTeam(bayern);
		match2.setAwayTeam(barcelona);
		match2.setDate(Date.from(Instant.from(dtf.parse("2019-12-24 18:00"))));
		match2.setLeague(league);
		
		if(match.getDate().after(from) && 
				match.getDate().before(to)) list.add(match);
		if(match.getDate().after(from) && 
				match.getDate().before(to)) list.add(match2);
		
		return list;
	}
	
	private League createLeague() {
		League league = FootballFactoryImpl.eINSTANCE.createLeague();
		league.setCode("MOCKLEAGUE");
		league.setName("Mock League");
		return league;
	}

	@Override
	public League getLeagueInfo() {
		return createLeague();
	}
}
