package com.tdt4250.footballproject.osgi.wc18.json.transform;

import java.io.IOException;
import java.net.URL;

import org.apache.commons.io.IOUtils;
import org.osgi.framework.FrameworkUtil;

import com.fasterxml.jackson.core.JsonParseException;
//import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
//import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
//import com.fasterxml.jackson.databind.SerializationFeature;
//import com.fasterxml.jackson.databind.node.ObjectNode;
import com.tdt4250.footballproject.osgi.wc18.json.model.WorldCup;

public class JacksonWorldCupMapper {
	/**
	 * Obtain a WorldCup object from an array of bytes.
	 * @param jsonData the array of bytes representing json data
	 * @return a WorldCup object represented by the data
	 * @throws JsonParseException The data is not json type.
	 * @throws JsonMappingException The data does not correspond to a WorldCup object.
	 * @throws IOException An I/O exception of some sort has occurred.
	 */
	public static WorldCup getWorldCup(byte[] jsonData) throws JsonParseException, JsonMappingException, IOException {
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		WorldCup worldCup = objectMapper.readValue(jsonData, WorldCup.class);
		
		return worldCup;

	}
	
	/**
	 * Obtain a WorldCup object from a json file.
	 * @param fileName the name of the json file
	 * @return a WorldCup object represented by the data in the file
	 * @throws JsonParseException The data is not json type.
	 * @throws JsonMappingException The data does not correspond to a WorldCup object.
	 * @throws IOException An I/O exception of some sort has occurred.
	 */
	public static WorldCup getWorldCup(String fileName) throws JsonParseException, JsonMappingException, IOException {
		URL url = FrameworkUtil.getBundle(JacksonWorldCupMapper.class)
				.getResource(fileName);
		byte[] jsonData = IOUtils.toByteArray(url.openStream());
		return getWorldCup(jsonData);
	}
}
