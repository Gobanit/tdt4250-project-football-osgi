package com.tdt4250.footballproject.osgi.wc18.json.model;

import java.util.List;

public class Group {
	private String name;
	private int winner;
	private int runnerup;
	private List<JMatch> matches;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getWinner() {
		return winner;
	}
	public void setWinner(int winner) {
		this.winner = winner;
	}
	public int getRunnerup() {
		return runnerup;
	}
	public void setRunnerup(int runnerup) {
		this.runnerup = runnerup;
	}
	public List<JMatch> getMatches() {
		return matches;
	}
	public void setMatches(List<JMatch> matches) {
		this.matches = matches;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		for (JMatch match : matches) {
			sb.append(match + "\n\t");
		}
		String matchesString = sb.toString();
		return "Group [name=" + name + ", winner=" + winner + ", runnerup=" + runnerup + ",\n\tmatches=" + matchesString + "]";
	}

	
}
