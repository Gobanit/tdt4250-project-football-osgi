package com.tdt4250.footballproject.osgi.wc18.json.model;

import java.util.List;

public class Round {
	private String name;
	private List<RoundMatch> matches;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<RoundMatch> getMatches() {
		return matches;
	}
	public void setMatches(List<RoundMatch> matches) {
		this.matches = matches;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		for (JMatch match : matches) {
			sb.append(match + "\n\t");
		}
		String matchesString = sb.toString();
		return "Round [name=" + name + ",\n\tmatches=" + matchesString + "]";
	}
	
}
