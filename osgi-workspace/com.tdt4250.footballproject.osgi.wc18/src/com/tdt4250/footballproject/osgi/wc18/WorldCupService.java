package com.tdt4250.footballproject.osgi.wc18;

import java.io.IOException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.osgi.service.component.annotations.Component;

import com.tdt4250.footballproject.model.football.League;
import com.tdt4250.footballproject.model.football.Match;
import com.tdt4250.footballproject.osgi.api.FootballService;
import com.tdt4250.footballproject.osgi.wc18.json.transform.Converter;

@Component(service = FootballService.class)
public class WorldCupService implements FootballService {
	private final String jsonFileName = "data/worldcup-2018.json";
	private final League league; 
	
	/**
	 * Create a FootballService for the WorldCup 2018
	 * @throws IOException The file worldcup-2018.json was not found or it had wrong data
	 */
	public WorldCupService() throws IOException {
		try {
			league = new Converter(jsonFileName).getLeague();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * Obtain the matches between two dates
	 * @param from the date you want the matches from
	 * @param to the date you want the matches to
	 * @return a list with the matches between that two dates
	 */
	@Override
	public List<Match> getMatches(Date from, Date to) {
		System.out.println(this.getClass()+" - getMatches WC18 "+from+", "+to);
		List<Match> matches = new LinkedList<Match>();
		for (Match match : league.getMatches()) {
			if (! (match.getDate().before(from) || match.getDate().after(to)))
				matches.add(match);
		}
		matches.sort((m1, m2) -> m2.getDate().compareTo(m1.getDate()));
		return matches;
	}

	@Override
	public League getLeagueInfo() {
		return league;
	}

}
