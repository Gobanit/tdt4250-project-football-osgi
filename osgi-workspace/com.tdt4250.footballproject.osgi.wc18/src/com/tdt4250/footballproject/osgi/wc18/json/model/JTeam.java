package com.tdt4250.footballproject.osgi.wc18.json.model;

public class JTeam {
	private int id;
	private String name;
	private String fifaCode;
	private String iso2;
	private String flag;
	private String emoji;
	private String emojiString;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFifaCode() {
		return fifaCode;
	}
	public void setFifaCode(String fifaCode) {
		this.fifaCode = fifaCode;
	}
	public String getIso2() {
		return iso2;
	}
	public void setIso2(String iso2) {
		this.iso2 = iso2;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getEmoji() {
		return emoji;
	}
	public void setEmoji(String emoji) {
		this.emoji = emoji;
	}
	public String getEmojiString() {
		return emojiString;
	}
	public void setEmojiString(String emojiString) {
		this.emojiString = emojiString;
	}
	
	@Override
	public String toString() {
		return "Team [id=" + id + ", name=" + name + ", fifaCode=" + fifaCode + ", iso2=" + iso2 + ", flag=" + flag
				+ ", emoji=" + emoji + ", emojiString=" + emojiString + "]";
	}
	
}
