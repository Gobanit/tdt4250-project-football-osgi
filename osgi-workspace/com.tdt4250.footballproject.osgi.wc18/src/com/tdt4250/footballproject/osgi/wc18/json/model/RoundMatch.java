package com.tdt4250.footballproject.osgi.wc18.json.model;

public class RoundMatch extends JMatch {
	private Integer home_penalty;
	private Integer away_penalty;
	private int winner;
	
	public Integer getHome_penalty() {
		return home_penalty;
	}
	public void setHome_penalty(Integer home_penalty) {
		this.home_penalty = home_penalty;
	}
	public Integer getAway_penalty() {
		return away_penalty;
	}
	public void setAway_penalty(Integer away_penalty) {
		this.away_penalty = away_penalty;
	}
	public int getWinner() {
		return winner;
	}
	public void setWinner(int winner) {
		this.winner = winner;
	}
	
	
}
