package com.tdt4250.footballproject.osgi.wc18.json.model;

import java.util.List;
import java.util.Map;

public class WorldCup {
	private List<Stadium> stadiums;
	private List<TvChannel> tvchannels;
	private List<JTeam> teams;
	private Map<String, Group> groups;
	private Map<String, Round> knockout;
	
	public List<Stadium> getStadiums() {
		return stadiums;
	}
	public void setStadiums(List<Stadium> stadiums) {
		this.stadiums = stadiums;
	}
	public List<TvChannel> getTvchannels() {
		return tvchannels;
	}
	public void setTvchannels(List<TvChannel> tvchannels) {
		this.tvchannels = tvchannels;
	}
	public List<JTeam> getTeams() {
		return teams;
	}
	public void setTeams(List<JTeam> teams) {
		this.teams = teams;
	}
	public Map<String, Group> getGroups() {
		return groups;
	}
	public void setGroups(Map<String, Group> groups) {
		this.groups = groups;
	}
	public Map<String, Round> getKnockout() {
		return knockout;
	}
	public void setKnockout(Map<String, Round> knockout) {
		this.knockout = knockout;
	}
	
	// It shows teams, groups and knockout
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("WorldCup [teams=[");
		for (JTeam team : teams)
			sb.append(team + "\n");
		sb.append("],\n");
		sb.append("groups=[");
		for (String groupName : groups.keySet()) {
			sb.append(groupName + "=[");
			Group group = groups.get(groupName);
			sb.append(group + "]\n");
		}
		sb.append("],\n");
		sb.append("knockout=[");
		for (String roundName : knockout.keySet()) {
			sb.append(roundName + "=[");
			Round round = knockout.get(roundName);
			sb.append(round + "]\n");
		}
		sb.append("]\n]");
		
		return sb.toString();
	}
	
}
