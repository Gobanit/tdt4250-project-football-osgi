package com.tdt4250.footballproject.osgi.wc18.json.model;

public class JMatch {
	private int name;
	private String type;
	private int home_team;
	private int away_team;
	private int home_result;
	private int away_result;
	private String date;
	private int stadium;
	private int[] channels;
	private boolean finished;
	private int matchday;
	
	public int getName() {
		return name;
	}
	public void setName(int name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getHome_team() {
		return home_team;
	}
	public void setHome_team(int home_team) {
		this.home_team = home_team;
	}
	public int getAway_team() {
		return away_team;
	}
	public void setAway_team(int away_team) {
		this.away_team = away_team;
	}
	public int getHome_result() {
		return home_result;
	}
	public void setHome_result(int home_result) {
		this.home_result = home_result;
	}
	public int getAway_result() {
		return away_result;
	}
	public void setAway_result(int away_result) {
		this.away_result = away_result;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public int getStadium() {
		return stadium;
	}
	public void setStadium(int stadium) {
		this.stadium = stadium;
	}
	public int[] getChannels() {
		return channels;
	}
	public void setChannels(int[] channels) {
		this.channels = channels;
	}
	public boolean isFinished() {
		return finished;
	}
	public void setFinished(boolean finished) {
		this.finished = finished;
	}
	public int getMatchday() {
		return matchday;
	}
	public void setMatchday(int matchday) {
		this.matchday = matchday;
	}
	
	@Override
	public String toString() {
		return "Match [name=" + name + ", type=" + type + ", home_team=" + home_team + ", away_team=" + away_team
				+ ", home_result=" + home_result + ", away_result=" + away_result + ", date=" + date + ", finished="
				+ finished + ", matchday=" + matchday + "]";
	}

}
