package com.tdt4250.footballproject.osgi.wc18.json.transform;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.tdt4250.footballproject.osgi.wc18.json.model.Group;
import com.tdt4250.footballproject.osgi.wc18.json.model.JMatch;
import com.tdt4250.footballproject.osgi.wc18.json.model.JTeam;
import com.tdt4250.footballproject.osgi.wc18.json.model.Round;
import com.tdt4250.footballproject.osgi.wc18.json.model.RoundMatch;
import com.tdt4250.footballproject.osgi.wc18.json.model.WorldCup;
import com.tdt4250.footballproject.model.football.FootballFactory;
import com.tdt4250.footballproject.model.football.League;
import com.tdt4250.footballproject.model.football.Match;
import com.tdt4250.footballproject.model.football.Team;

public class Converter {
	private final WorldCup worldCup;
	private final Map<Integer, Team> idTeams = new HashMap<Integer, Team>();
	private final Map<Integer, Match> nameMatches = new HashMap<Integer, Match>();
	private final int DEFAULT_POINTS = 40;
	private int maxPoints = DEFAULT_POINTS;
	
	
	/**
	 * Create a Converter from a WorldCup
	 * @param  worldCup the WorldCup object
	 */
	public Converter(WorldCup worldCup) {
		this.worldCup = worldCup;
	}
	
	/**
	 * Create a Converter from an array of bytes
	 * @param jsonData array of bytes with the json data
	 * @throws IOException if the data is not correct.
	 */
	public Converter(byte[] jsonData) throws IOException {
		this.worldCup = JacksonWorldCupMapper.getWorldCup(jsonData);
	}
	
	/**
	 * Create a Converter from a json file.
	 * @param jsonFileName the name of the json file
	 * @throws IOException if the file does not exist or the data in the file is not correct.
	 */
	public Converter(String jsonFileName) throws IOException {
		this.worldCup = JacksonWorldCupMapper.getWorldCup(jsonFileName);
	}
	
	/**
	 * Obtain a League of the EMF model.
	 * The league is transformed from a WorldCup object
	 * @return the league
	 */
	public League getLeague() {
		League league = FootballFactory.eINSTANCE.createLeague();
		
		league.setName("World Cup - 2018");
		league.setCode("WC18");
		league.setCountry("World");
		
		for (JTeam jTeam : worldCup.getTeams()) {
			Team team = getTeam(jTeam);
			
			league.getTeams().add(team);
			
		}
		
		for (Group group : worldCup.getGroups().values()) {
			for (JMatch jMatch : group.getMatches()) {
				Match match = getMatch(jMatch);
				
				league.getMatches().add(match);
				
			}
		}
		
		for (Round round : worldCup.getKnockout().values()) {
			for (JMatch jMatch : round.getMatches()) {
				Match match = getMatch(jMatch);
				
				league.getMatches().add(match);
				
			}
		}
		
		return league;
	}
	
	private Match getMatch (JMatch jMatch) {
		Match match = FootballFactory.eINSTANCE.createMatch();
		nameMatches.put(jMatch.getName(), match);
		
		match.setHomeTeam(idTeams.get(jMatch.getHome_team()));
		match.setAwayTeam(idTeams.get(jMatch.getAway_team()));
		match.setHomeTeamScore(jMatch.getHome_result());
		match.setAwayTeamScore(jMatch.getAway_result());
		
		if (match.getAwayTeamScore() > match.getHomeTeamScore()) {
			match.setWinner(match.getAwayTeam());
		} else if(match.getAwayTeamScore() < match.getHomeTeamScore()) {
			match.setWinner(match.getHomeTeam());
		}
		
		
		String dateStr = jMatch.getDate();
		try {
			dateStr = dateStr.substring(0, 22) + dateStr.substring(23);	// to get rid of the ":"
			Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").parse(dateStr);
			match.setDate(date);
		} catch (Exception e) {
		}
		
		boolean isFinal = jMatch.getClass() == RoundMatch.class;
		
		// This is for updating the score
		if (match.getAwayTeamScore() != match.getHomeTeamScore()) {
			Team winner = match.getWinner();
			Team loser = (match.getAwayTeam().equals(winner) ? match.getHomeTeam() : match.getAwayTeam());
			
			int matchResult = 3;	// 3 for victory, 0 for defeat
			double OpponentsStrength = loser.getPoints() / ((double) maxPoints);
			double importanceOfMatch = (isFinal ? 2.5 : 2.0);
			
			int matchPoints = (int) (matchResult * OpponentsStrength * importanceOfMatch);
			winner.setPoints(winner.getPoints() + matchPoints);
			
		} else {
			int matchResult = 1; // 1 for draw
			double awayStrength = match.getAwayTeam().getPoints() / ((double) maxPoints);
			double homeStrength = match.getHomeTeam().getPoints() / ((double) maxPoints);
			double importanceOfMatch = (isFinal ? 2.5 : 2.0);
			
			int homeMatchPoints = (int) (matchResult * awayStrength * importanceOfMatch);
			int awayMatchPoints = (int) (matchResult * homeStrength * importanceOfMatch);
			match.getHomeTeam().setPoints(match.getHomeTeam().getPoints() + homeMatchPoints);
			match.getAwayTeam().setPoints(match.getAwayTeam().getPoints() + awayMatchPoints);
			
		}
		
		
		if (match.getAwayTeam().getPoints() > maxPoints)
			maxPoints = match.getAwayTeam().getPoints();
		
		if (match.getHomeTeam().getPoints() > maxPoints)
			maxPoints = match.getHomeTeam().getPoints();
		
		return match;
	}
	
	private Team getTeam (JTeam jTeam) {
		Team team = FootballFactory.eINSTANCE.createTeam();
		idTeams.put(jTeam.getId(), team);
		
		// We only have information about the name
		team.setName(jTeam.getName());
		
		team.setPoints(DEFAULT_POINTS);
		
		return team;
	}
	
	
}
