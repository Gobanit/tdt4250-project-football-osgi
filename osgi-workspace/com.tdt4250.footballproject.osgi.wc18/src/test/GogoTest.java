package test;

import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.component.annotations.Component;

import com.tdt4250.footballproject.model.football.Match;
import com.tdt4250.footballproject.osgi.api.FootballService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.felix.service.command.Descriptor;

@Component(
		service = GogoTest.class,
		property = {
			"osgi.command.scope=footballService",
			"osgi.command.function=listLeagues",
			"osgi.command.function=matches"
		}
	)
public class GogoTest {
	
	@Descriptor("list available leagues")
	public void listLeagues() {
		System.out.print("Leagues: ");
		BundleContext bc = FrameworkUtil.getBundle(this.getClass()).getBundleContext();
		try {
			for (ServiceReference<FootballService> serviceReference : bc.getServiceReferences(FootballService.class, null)) {
				FootballService service = bc.getService(serviceReference);
					if (service != null) {
						System.out.print(service.getLeagueInfo().getCode());
					}
				System.out.print(" ");
			}
		} catch (InvalidSyntaxException e) {
		}
		System.out.println();
	}
	
	@Descriptor("show the matches from one league")
	public void matches(
			@Descriptor("the name of the league")
			String leagueName
			) {
		matches(leagueName, "1900-01-01", "2100-01-01");
	}
	
	@Descriptor("show the matches from one league")
	public void matches(
			@Descriptor("the name of the league")
			String leagueName,
			@Descriptor("the date you want matches from (yyyy-MM-dd)")
			String from,
			@Descriptor("the date you want matches to (yyyy-MM-dd)")
			String to
			) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		try {
			Date dateFrom = format.parse(from + "T00:00:00");
			Date dateTo = format.parse(to + "T23:59:59");
			matches(leagueName, dateFrom, dateTo);
		} catch (ParseException e) {
			System.err.println("The format of the dates is not correct");
		}
	}
	

	private void matches(String leagueName, Date from, Date to) {
		FootballService footballService = null;
		BundleContext bc = FrameworkUtil.getBundle(this.getClass()).getBundleContext();
		try {
			for (ServiceReference<FootballService> serviceReference : bc.getServiceReferences(FootballService.class, null)) {
				FootballService service = bc.getService(serviceReference);
				if (leagueName.equals(service.getLeagueInfo().getCode())) {
					footballService = service;
					break;
				}
				
			}
			if (footballService != null) {
				System.out.println("Matches:");
				for (Match match : footballService.getMatches(from, to)) {
					System.out.println(string(match));
				}
			} else {
				System.err.println("The league does not exist");
			}
		} catch (InvalidSyntaxException e) {
			System.err.println("Wrong Syntax when calling matches");
		}
		System.out.println();
	}
	
	private String string(Match match) {
		return "Match: [" + "Teams: " + match.getHomeTeam().getName() + ", " + match.getAwayTeam().getName()
					+ ", score: " + match.getHomeTeamScore() + " - " + match.getAwayTeamScore() 
					+ /*", winner: " + match.getWinner().getName() +*/ ", date: " + match.getDate() + "]";
	}
	
}
