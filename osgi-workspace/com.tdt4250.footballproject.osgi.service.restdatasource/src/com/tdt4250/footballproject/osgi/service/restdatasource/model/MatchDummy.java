package com.tdt4250.footballproject.osgi.service.restdatasource.model;

import java.util.Date;

public class MatchDummy {

	private Integer id;
	private String status;
	private TeamRef homeTeam;
	private TeamRef awayTeam;
	private Date utcDate;
	private ScoreDummy score;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public TeamRef getHomeTeam() {
		return homeTeam;
	}

	public void setHomeTeam(TeamRef homeTeam) {
		this.homeTeam = homeTeam;
	}

	public TeamRef getAwayTeam() {
		return awayTeam;
	}

	public void setAwayTeam(TeamRef awayTeam) {
		this.awayTeam = awayTeam;
	}

	public Date getUtcDate() {
		return utcDate;
	}

	public void setUtcDate(Date utcDate) {
		this.utcDate = utcDate;
	}

	public ScoreDummy getScore() {
		return score;
	}

	public void setScore(ScoreDummy score) {
		this.score = score;
	}

}
