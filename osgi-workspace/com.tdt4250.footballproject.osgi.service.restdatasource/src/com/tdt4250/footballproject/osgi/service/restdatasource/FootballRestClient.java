package com.tdt4250.footballproject.osgi.service.restdatasource;

import java.util.Date;

/**
 * Interface for making the actual REST requests
 *
 */
public interface FootballRestClient {

	/**
	 * Executes get matches request, returning the json
	 * @param competitionId
	 * @param from
	 * @param to
	 * @return matches response json
	 */
	String getMatches(String competitionId, Date from, Date to);
	
	/**
	 * Executes get competitions request
	 * @param id - competition id
	 * @return competition response json
	 */
	String getCompetition(String id);

}