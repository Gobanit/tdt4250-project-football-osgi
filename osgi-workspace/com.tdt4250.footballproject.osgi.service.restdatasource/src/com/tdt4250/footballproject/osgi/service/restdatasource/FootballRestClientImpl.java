package com.tdt4250.footballproject.osgi.service.restdatasource;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.osgi.service.component.annotations.Component;

@Component
public class FootballRestClientImpl implements FootballRestClient {
	private static final String API_URL = "http://api.football-data.org/v2/";
	private static final String TOKEN = "6ed4fd7b3da3402498b0d6751f2991eb";
	private static final Date SOONEST_POSSIBLE_DATE;
	static {
		Date d = null;
		try {
			d = new SimpleDateFormat("yyyy-MM-dd").parse("2017-01-01");
		} catch (ParseException e) {
			e.printStackTrace();
		} finally {
			SOONEST_POSSIBLE_DATE = d;
		}
		
	}
			
	
	@Override
	public String getMatches(String competitionId, Date from, Date to) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		
		// api refuses queries before some date
		if(from.before(SOONEST_POSSIBLE_DATE)) {
			from = SOONEST_POSSIBLE_DATE;
			System.out.println(this.getClass().getSimpleName() + 
					" - had to change 'from' date to "+from+", because it was too soon");
		}
		
		// api refuses more than 20 days
		long maxDiff = 1000*60*60*24*20;
		if(to.getTime() - from.getTime() > maxDiff) {
			to.setTime(from.getTime()+maxDiff);
			System.out.println(this.getClass().getSimpleName() + 
					" - had to change 'to' date to "+to);
		}
		
		
		List<NameValuePair> nvps = new ArrayList<>();
		nvps.add(new BasicNameValuePair("dateFrom", sdf.format(from)));
		nvps.add(new BasicNameValuePair("dateTo", sdf.format(to)));
		
		return executeRequestWrapper("competitions/"+competitionId+"/matches/", nvps);
	}
	
	
	@Override
	public String getCompetition(String id) {
		return executeRequestWrapper("competitions/"+id+"/", null);
	}
	
	
	private String executeRequestWrapper(String path, List<NameValuePair> nvps) {
		try {
			return executeRequest(path, nvps);
		} catch (IOException | URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	private String executeRequest(String path, List<NameValuePair> nvps) throws IOException, URISyntaxException {
		if(nvps == null) nvps = new ArrayList<>();
		
		CloseableHttpClient httpclient = HttpClients.createDefault();
		
		URI uri = new URIBuilder(API_URL+path)
				.addParameter("plan", "TIER_ONE")
				.addParameters(nvps)
				.build();
			
		HttpGet httpGet = new HttpGet(uri);
		httpGet.addHeader("X-Auth-Token", TOKEN);		
		
		
		System.out.println("Request GET: "+httpGet.getURI());
		CloseableHttpResponse response1 = httpclient.execute(httpGet);
		
		try {
		    System.out.println(response1.getStatusLine());
		    
		    // Too many requests
		    if(response1.getStatusLine().getStatusCode() == 429) {
		    	throw new RuntimeException("Too many requests for free API!");
		    }
		    
		    HttpEntity entity = response1.getEntity();
		    
		    String jsonResp = IOUtils.toString(entity.getContent(), 
		    		StandardCharsets.UTF_8.name());
		    
		    EntityUtils.consume(entity);
		    
		    System.out.println("Response: "+jsonResp);
		    return jsonResp;
		} catch(IOException e) {
			throw e;
		} finally {
		    response1.close();
		}
		
	}

}
