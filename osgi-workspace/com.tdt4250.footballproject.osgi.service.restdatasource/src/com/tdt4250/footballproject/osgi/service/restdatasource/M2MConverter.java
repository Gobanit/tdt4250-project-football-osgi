package com.tdt4250.footballproject.osgi.service.restdatasource;

import java.util.List;
import java.util.stream.Collectors;

import com.tdt4250.footballproject.model.football.FootballFactory;
import com.tdt4250.footballproject.model.football.League;
import com.tdt4250.footballproject.model.football.Match;
import com.tdt4250.footballproject.model.football.Team;
import com.tdt4250.footballproject.model.football.impl.FootballFactoryImpl;
import com.tdt4250.footballproject.osgi.service.restdatasource.model.MatchDummy;
import com.tdt4250.footballproject.osgi.service.restdatasource.model.TeamRef;

/**
 * Class responsible for mapping "local" model classes, to ecore model classes
 *
 */
public class M2MConverter {
	
	private FootballFactory fact = FootballFactoryImpl.eINSTANCE;
	private MappingContext context = new MappingContext();
	
	/**
	 * Maps dummy match to model match
	 * @param m - dummy match
	 * @return model match
	 */
	public Match matchToMatch(MatchDummy m) {
		if(context.matches.containsKey(m.getId())) 
			return context.matches.get(m.getId());
		
		Match match = fact.createMatch();
		
		match.setHomeTeam(convertTeamRefToTeam(m.getHomeTeam()));
		match.setAwayTeam(convertTeamRefToTeam(m.getAwayTeam()));
		match.setDate(m.getUtcDate());
		match.setHomeTeamScore(m.getScore().getFinalHomeTeamGoals());
		match.setAwayTeamScore(m.getScore().getFinalAwayTeamGoals());
		Team winner;
		if(m.getScore() != null && m.getScore().getWinner() != null) {
			switch (m.getScore().getWinner()) {
				case HOME_TEAM: winner = match.getHomeTeam(); break;
				case AWAY_TEAM: winner = match.getAwayTeam(); break;
				default: winner = null;
			}
			match.setWinner(winner);
		}
		
		
		return match;
		

	}
	
	/**
	 * Maps the list of dummy matches to model matches
	 * @param matches
	 * @param league
	 * @return list of model matches
	 */
	public List<Match> matchToMatchList(List<MatchDummy> matches, League league) {
		return matches.stream()
				.map((m) -> { 
					Match match = matchToMatch(m); 
					match.setLeague(league); 
					return match;
					})
				.collect(Collectors.toList());
	}
	
	/**
	 * Maps team ref to model team
	 * @param tr - team reference
	 * @return model team
	 */
	public Team convertTeamRefToTeam(TeamRef tr) {
		if(context.teams.containsKey(tr.getId())) 
			return context.teams.get(tr.getId());
		
		Team team = fact.createTeam();
		team.setName(tr.getName());

		return team;
	}
}
