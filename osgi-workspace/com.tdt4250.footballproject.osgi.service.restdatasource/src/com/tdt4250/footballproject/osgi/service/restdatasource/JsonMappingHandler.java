package com.tdt4250.footballproject.osgi.service.restdatasource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tdt4250.footballproject.osgi.service.restdatasource.model.CompetitionDummy;
import com.tdt4250.footballproject.osgi.service.restdatasource.model.MatchDummy;

/**
 * Class responsible for mapping json responses to java classes
 *
 */
public class JsonMappingHandler {

	/**
	 * Parses list of "dummy" matches
	 * @param json
	 * @return list of dummy matches
	 * @throws IOException
	 */
	public List<MatchDummy> readMatchListFromMatchResponse(String json) throws IOException {
		ObjectMapper mapper = createMapper();
		
		JsonNode node = readAsNode(mapper, json);
	    
		node = node.get("matches");
		json = mapper.writeValueAsString(node);
		
		System.out.println("MatchesNode: "+json);

		List<MatchDummy> matches = mapper.readValue(json, mapper.getTypeFactory().
					constructCollectionType(ArrayList.class, MatchDummy.class));
		
		return matches;	
	}
	
	/**
	 * Parses CompetitionDummy from json
	 * @param json
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public CompetitionDummy readCompetition(String json) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = createMapper();
		return mapper.readValue(json, CompetitionDummy.class);
	}
	
	private ObjectMapper createMapper() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		
		return mapper;
	}

	private JsonNode readAsNode(ObjectMapper mapper, String json) {
		try {
			JsonFactory factory = mapper.getFactory();
	    	JsonParser parser = factory.createParser(json);
			return mapper.readTree(parser);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
		
	
}
