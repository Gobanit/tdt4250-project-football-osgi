package com.tdt4250.footballproject.osgi.service.restdatasource;

import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;
import java.util.stream.Collectors;

import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;

import com.tdt4250.footballproject.model.football.Match;
import com.tdt4250.footballproject.osgi.api.FootballService;

/**
 * Class responsible for user console interaction. 
 * Enables basic commands for ading, removing and showing services
 *
 */
@Component(
		service = RestDataSourceConsole.class,
		property = {
			"osgi.command.scope=restdataconsole",
			"osgi.command.function=addService",
			"osgi.command.function=removeService",
			"osgi.command.function=listServices",
			"osgi.command.function=test",
		}
	)
public class RestDataSourceConsole {
	private static final int SERVICE_INIT_WAIT_ITERATIONS = 5;
	private static final int SERVICE_INIT_WAIT_ITER_DELAY = 10;
	
	@Reference(cardinality = ReferenceCardinality.MANDATORY)
	private ConfigurationAdmin cm;
	
	public void addService(String competitionId) throws IOException {
		System.out.println("Add service");
		
		boolean modif = true;
		
		Configuration config = getConfig(competitionId);
		if (config == null) {
			// create a new one
			config = cm.createFactoryConfiguration(RestAPIDatasourceService.FACTORY_PID, "?");
			modif = false;
		}
		
		Dictionary<String, String> props = new Hashtable<>();
		props.put(RestAPIDatasourceService.SERV_COMPETITION_PROP, competitionId);
		config.update(props);
			
		
		FootballService service = null;
		
		// waiting for service to be initialized
		for(int i=0;i<SERVICE_INIT_WAIT_ITERATIONS;i++) {
			service = getServices().stream().
				filter((s) -> { return s.getCompetitionId() == competitionId; })
				.findFirst().orElse(null);
			
			if(service != null) break;
			
			try {
				Thread.sleep(SERVICE_INIT_WAIT_ITER_DELAY);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		if(modif) System.out.println("Modified service "+service);
		else System.out.println("Create service "+service);
	}
	
	public void removeService(String competitionId) throws IOException {
		System.out.println("Remove service");
		Configuration config = getConfig(competitionId);
		if(config == null) {
			System.out.println("Service "+competitionId+" not present");
			return;
		}
		
		config.delete();
		System.out.println("Service "+competitionId+" deleted");
	}
	
	public void listServices() {
		System.out.println("List services!");
		for(FootballService fs : getServices()) {
			System.out.println(fs);
		}
	}
	
	private List<RestAPIDatasourceService> getServices() {
		BundleContext bc = FrameworkUtil.getBundle(this.getClass()).getBundleContext();		
		try {
			Collection<ServiceReference<FootballService>> refs = 
					bc.getServiceReferences(FootballService.class, (String) null);

			return refs.stream()
					.map((ref) -> { return bc.getService(ref); })
					.filter((serv) -> { return serv instanceof RestAPIDatasourceService; })
					.map((serv) -> { return (RestAPIDatasourceService) serv; })
					.collect(Collectors.toList());
			
		} catch (InvalidSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return Collections.emptyList();
	}
	
	public void test() {
		try {
			testBody();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	private void testBody() {
		System.out.println("Test start!");
		
		BundleContext bc = FrameworkUtil.getBundle(this.getClass()).getBundleContext();		
		FootballService service = bc.getService(bc.getServiceReference(FootballService.class));
		
		System.out.println("Service: "+service);
		
		System.out.println("Match block: ");
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm").withZone(ZoneId.systemDefault());
		Date from = Date.from(Instant.from(dtf.parse("2019-10-01 18:00")));
		Date to = Date.from(Instant.from(dtf.parse("2019-10-06 18:00")));

		System.out.println("Before matches call");
		List<Match> list = service.getMatches(from, to);
		System.out.println("MATCHES!");
		for(Match m : list) {
			System.out.println(m);
		}	
		
		System.out.println("Test end!");		
	}
	
	
	private Configuration getConfig(String competitionId) {
		Configuration[] configs;
		try {
			configs = cm.listConfigurations("(&(" + RestAPIDatasourceService.SERV_COMPETITION_PROP + "=" + competitionId + ")(service.factoryPid=" + RestAPIDatasourceService.FACTORY_PID + "))");
			if (configs != null && configs.length >= 1) {
				return configs[0];
			}
		} catch (IOException | org.osgi.framework.InvalidSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	
	
}
	
	