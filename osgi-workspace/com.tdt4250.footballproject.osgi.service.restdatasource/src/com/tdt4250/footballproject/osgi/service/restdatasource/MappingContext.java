package com.tdt4250.footballproject.osgi.service.restdatasource;

import java.util.HashMap;
import java.util.Map;

import com.tdt4250.footballproject.model.football.Match;
import com.tdt4250.footballproject.model.football.Team;

/**
 * Class for holding model object instances while mapping is executed
 *
 */
public class MappingContext {

	public Map<Integer, Team> teams = new HashMap<>();
	public Map<Integer, Match> matches = new HashMap<>();

}
