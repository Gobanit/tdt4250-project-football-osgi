package com.tdt4250.footballproject.osgi.service.restdatasource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.osgi.framework.BundleContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.osgi.service.log.Logger;
import org.osgi.service.log.LoggerFactory;

import com.tdt4250.footballproject.model.football.FootballFactory;
import com.tdt4250.footballproject.model.football.League;
import com.tdt4250.footballproject.model.football.Match;
import com.tdt4250.footballproject.osgi.api.FootballService;
import com.tdt4250.footballproject.osgi.service.restdatasource.model.CompetitionDummy;
import com.tdt4250.footballproject.osgi.service.restdatasource.model.MatchDummy;

/**
 * Implementation of {@link FootballService} handling public REST datasource
 *
 */
@Component(
		configurationPid = RestAPIDatasourceService.FACTORY_PID,
		configurationPolicy = ConfigurationPolicy.REQUIRE
		)
public class RestAPIDatasourceService implements FootballService {
	public static final String FACTORY_PID = "tdt4250.footballproject.RestAPIDatasourceService";
	public static final String SERV_COMPETITION_PROP = "competitionId";
	
	private String competitionId;
	private final JsonMappingHandler jsonHandler;
	private League leagueCache;
	
	@Reference(cardinality = ReferenceCardinality.MANDATORY)
	public FootballRestClient client;

	@Reference(cardinality = ReferenceCardinality.MANDATORY, policy = ReferencePolicy.DYNAMIC)
	public volatile LoggerFactory loggerFactory;

	
	public RestAPIDatasourceService() {
		this.jsonHandler = new JsonMappingHandler();
	}
	
	public @interface RestApiDatasourceServiceConfig {
		String competitionId();
	}
	
	@Activate
	public void activate(BundleContext bc, RestApiDatasourceServiceConfig config) {
		update(bc, config);
	}

	@Modified
	public void modify(BundleContext bc, RestApiDatasourceServiceConfig config) {
		System.out.println(this+" Modify");
		update(bc, config);		
	}
	
	protected void update(BundleContext bc, RestApiDatasourceServiceConfig config) {
		System.out.println(this+" Update");
		this.competitionId = config.competitionId();
	}
	
	private Logger getLogger() {
		if (loggerFactory != null) {
			return loggerFactory.getLogger(RestAPIDatasourceService.class);
		}
		return null;
	}

	
	@Override
	public League getLeagueInfo() {
		return getLeague();
	}
	
	private League getLeague() {
		if(leagueCache == null) loadLeagueCache();		
		return leagueCache;
	}
	
	private void loadLeagueCache() {
		String json = client.getCompetition(competitionId);

		try {
			CompetitionDummy comp = jsonHandler.readCompetition(json);
			leagueCache = FootballFactory.eINSTANCE.createLeague();
			leagueCache.setCode("COMPETITION__"+comp.getId());
			leagueCache.setName(comp.getName());
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			leagueCache = null;
		}

	}

	@Override
	public List<Match> getMatches(Date from, Date to) {
		System.out.println(this+" getMatches "+from+" "+to);
		getLogger().info("log getMatches "+from+" "+to);
		
		M2MConverter converter = new M2MConverter();
		
		String json = client.getMatches(competitionId, from, to);
		List<MatchDummy> matches = null;

		try {
			matches = jsonHandler.readMatchListFromMatchResponse(json);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ArrayList<>();
		}

		return converter.matchToMatchList(matches, copy(getLeague()));

	}

	private League copy(League league) {
		League l = FootballFactory.eINSTANCE.createLeague();
		League cache = getLeague();
		l.setCode(cache.getCode());
		l.setName(cache.getName());
		l.setCode(cache.getCountry());
		l.setIconUrl(cache.getIconUrl());
		return l;
	}

	@Override
	public String toString() {
		return this.getClass() + ":" + competitionId;
	}

	public FootballRestClient getClient() {
		return client;
	}

	public void setClient(FootballRestClient client) {
		this.client = client;
	}

	public String getCompetitionId() {
		return competitionId;
	}

}
