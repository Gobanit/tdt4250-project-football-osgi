package com.tdt4250.footballproject.osgi.service.restdatasource.model;

public class ScoreDummy {
	
	private MatchWinner winner;
	private SemiScoreDummy halfTime;
	private SemiScoreDummy fullTime;
	private SemiScoreDummy extraTime;
	
	public MatchWinner getWinner() {
		return winner;
	}
	public void setWinner(MatchWinner winner) {
		this.winner = winner;
	}
	public SemiScoreDummy getHalfTime() {
		return halfTime;
	}
	public void setHalfTime(SemiScoreDummy halfTime) {
		this.halfTime = halfTime;
	}
	public SemiScoreDummy getFullTime() {
		return fullTime;
	}
	public void setFullTime(SemiScoreDummy fullTime) {
		this.fullTime = fullTime;
	}
	
	public int getFinalHomeTeamGoals() {
		if(extraTime != null && extraTime.getHomeTeam() != null) return extraTime.getHomeTeam();
		if(fullTime != null && fullTime.getHomeTeam() != null) return fullTime.getHomeTeam();
		if(halfTime != null && halfTime.getHomeTeam() != null) return halfTime.getHomeTeam();
		return 0;
	}
	
	public int getFinalAwayTeamGoals() {
		if(extraTime != null && extraTime.getAwayTeam() != null) return extraTime.getAwayTeam();
		if(fullTime != null && fullTime.getAwayTeam() != null) return fullTime.getAwayTeam();
		if(halfTime != null && halfTime.getAwayTeam() != null) return halfTime.getAwayTeam();
		return 0;
	}

	
}

class SemiScoreDummy {
	private Integer homeTeam;
	private Integer awayTeam;
	public Integer getHomeTeam() {
		return homeTeam;
	}
	public void setHomeTeam(Integer homeTeam) {
		this.homeTeam = homeTeam;
	}
	public Integer getAwayTeam() {
		return awayTeam;
	}
	public void setAwayTeam(Integer awayTeam) {
		this.awayTeam = awayTeam;
	}
}