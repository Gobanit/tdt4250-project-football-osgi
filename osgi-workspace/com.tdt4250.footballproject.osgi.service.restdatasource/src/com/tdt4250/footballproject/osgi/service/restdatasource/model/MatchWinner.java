package com.tdt4250.footballproject.osgi.service.restdatasource.model;

public enum MatchWinner {
	HOME_TEAM, AWAY_TEAM, DRAW
}
