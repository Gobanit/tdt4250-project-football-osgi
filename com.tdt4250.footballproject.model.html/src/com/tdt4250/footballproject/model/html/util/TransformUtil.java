package com.tdt4250.footballproject.model.html.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.commons.io.FileUtils;
import org.eclipse.acceleo.engine.service.AbstractAcceleoGenerator;
import org.eclipse.emf.common.util.BasicMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

import com.tdt4250.footballproject.model.html.main.GeneratorsConfig;

/**
 * Class responsible for actual transformation using generators.
 *
 */
public class TransformUtil {

	private static final String TMP_DIR = "tmp_dir";
	private static final String TMP_RES_PATH = TMP_DIR+"/tmp_resource.xmi";
	private static final String TMP_OUT_DIR_PATH = TMP_DIR+"/tmp_output";
	
	private static final Lock lock = new ReentrantLock();

	private final EObject object;
	private final AbstractAcceleoGenerator gen;
	private final String outputFileName;
	
	/**
	 * Constructor for the transform util
	 * @param e - model instance object
	 * @param gen - generator class, with the same name as .mtl file
	 * @param outputFileName - name of the output file specified in .mtl file
	 */
	public TransformUtil(EObject e, AbstractAcceleoGenerator gen, String outputFileName) {
		this.object = e;
		this.gen = gen;
		this.outputFileName = outputFileName;
	}

	/**
	 * Runs acceleo transformation and returns the content of the output text file
	 * @return
	 */
	public String instance2Text() {
		// since they are using the same tmp directory, it needs to be synchronized on static lock
		synchronized(lock) {
			try {
				createXMI(object);
				transform(gen);
				return loadOutput(outputFileName);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} finally {
				try {
					deleteTmp();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
		return null;
		

	}

	private void deleteTmp() throws IOException {
		File f = new File(TMP_DIR);
		FileUtils.deleteDirectory(f);
	}

	private void transform(AbstractAcceleoGenerator gen) throws IOException {
		URI modelURI = URI.createFileURI(TMP_RES_PATH);

		List<String> props = new ArrayList<String>();
		File f = new File(TMP_OUT_DIR_PATH);
		f.mkdirs();
		
		initializeGenerator(gen, modelURI, f, props);

		BasicMonitor mon = new BasicMonitor();
		gen.doGenerate(mon);
	}

	private void initializeGenerator(AbstractAcceleoGenerator gen2, URI modelURI, File f, List<String> props) throws IOException {
		try {
			gen.initialize(modelURI, f, props);
		} catch(IOException e) {
			// probably cannot find .emtl file
			System.out.println("Probably .emtl is in bin directory, trying again");
			GeneratorsConfig.moduleInBin = true;
			try {
				gen.initialize(modelURI, f, props);
			} catch (IOException e1) {
				throw e1;
			} finally {
				GeneratorsConfig.moduleInBin = false;
			}
		}
	}

	private String loadOutput(String outputFileName) throws IOException {
		File result = new File(TMP_OUT_DIR_PATH+"/"+outputFileName);
		String content = FileUtils.readFileToString(result,	"UTF-8");

		return content;
	}

	private void createXMI(EObject e) throws IOException {
		Resource resource = new XMIResourceImpl(URI.createURI(TMP_RES_PATH));
		resource.getContents().add(e);

		ResourceSet resourceSet = new ResourceSetImpl();
		resourceSet.getResources().add(resource);
		Map<Object, Object> options = new HashMap<Object, Object>();
		options.put(XMIResource.OPTION_SCHEMA_LOCATION, Boolean.TRUE);

		resource.save(options);
	}
}
