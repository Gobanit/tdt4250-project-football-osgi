package com.tdt4250.footballproject.model.html.util;

import com.tdt4250.footballproject.model.footballProvider.LeagueChooser;
import com.tdt4250.footballproject.model.footballProvider.MatchesListResult;
import com.tdt4250.footballproject.model.html.main.MatchListToHTML;
import com.tdt4250.footballproject.model.html.main.UserInputHTML;

/**
 * Class wrapping the transformation methods for more clearer calls from outside
 *
 */
public class FootballTransformations {

	public static String matchesResultsToHTML(MatchesListResult mlr) {
		return new TransformUtil(mlr, new MatchListToHTML(), "matchlist.html").instance2Text();
	}
	
	public static String leaguesChooserToHtml(LeagueChooser lc) {
		return new TransformUtil(lc, new UserInputHTML(), "userinput.html").instance2Text();
	}
}
