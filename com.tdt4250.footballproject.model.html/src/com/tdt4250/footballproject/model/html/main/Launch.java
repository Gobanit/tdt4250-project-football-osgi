package com.tdt4250.footballproject.model.html.main;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import org.apache.commons.io.FileUtils;

import com.tdt4250.footballproject.model.football.FootballFactory;
import com.tdt4250.footballproject.model.football.League;
import com.tdt4250.footballproject.model.football.Match;
import com.tdt4250.footballproject.model.football.Team;
import com.tdt4250.footballproject.model.football.impl.FootballFactoryImpl;
import com.tdt4250.footballproject.model.footballProvider.FootballProviderFactory;
import com.tdt4250.footballproject.model.footballProvider.LeagueChooser;
import com.tdt4250.footballproject.model.footballProvider.MatchesListResult;
import com.tdt4250.footballproject.model.footballProvider.impl.FootballProviderFactoryImpl;
import com.tdt4250.footballproject.model.html.util.FootballTransformations;

/**
 * Class for development phase purposes
 *
 */
public class Launch {

	public static void main(String[] args) throws IOException {
		
		String output = FootballTransformations.matchesResultsToHTML(createMatchesListResult());
		System.out.println(output);

		
		FileUtils.write(new File("./output.html"), output, "UTF-8");
		
		output = FootballTransformations.leaguesChooserToHtml(createLeagueChooser());
		System.out.println(output);
		FileUtils.write(new File("./output2.html"), output, "UTF-8");
		
	}

	private static LeagueChooser createLeagueChooser() {
		FootballProviderFactory fact = FootballProviderFactoryImpl.eINSTANCE;
		LeagueChooser lc = fact.createLeagueChooser();
		
		FootballFactory fact2 = FootballFactoryImpl.eINSTANCE;
		
		League league = fact2.createLeague();
		league.setCountry("Slovakia");
		league.setName("Corgoň liga");
		league.setCode("SK_CRG_LIG");
		lc.getLeagues().add(league);
		
		league = fact2.createLeague();
		league.setCountry("Czechia");
		league.setName("1. Česká fotbalová liga");
		league.setCode("CZ_1_LIG");
		lc.getLeagues().add(league);
		
		return lc;
	}

	private static MatchesListResult createMatchesListResult() {
		FootballProviderFactory fact = FootballProviderFactoryImpl.eINSTANCE;
		MatchesListResult mlr = fact.createMatchesListResult();
		League l = createLeague();
		mlr.getLeagues().add(l);
		mlr.setStartDate(Date.from(Instant.now()));
		mlr.setEndDate(Date.from(Instant.now().plusSeconds(3600*24*20)));
		mlr.getFilteredMatches().addAll(l.getMatches());
		return mlr;
	}
	
	private static League createLeague() {
		FootballFactory fact = FootballFactoryImpl.eINSTANCE;
		League league = fact.createLeague();
		league.setCountry("Slovakia");
		league.setName("Corgoň liga");
		league.setCode("SK_CRG_LIG");
		
		Team inter = fact.createTeam();
		inter.setName("Inter Bratislava");
		
		Team slovan = fact.createTeam();
		slovan.setName("Slovan Bratislava");
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm").withZone(ZoneId.systemDefault());
		
		Match match = fact.createMatch();
		match.setHomeTeam(inter);
		match.setAwayTeam(slovan);
		match.setDate(Date.from(Instant.from(dtf.parse("2019-12-14 18:00"))));
		
		Match match2 = fact.createMatch();
		match2.setHomeTeam(slovan);
		match2.setAwayTeam(inter);
		match2.setDate(Date.from(Instant.from(dtf.parse("2019-12-15 18:00"))));
		
		league.getTeams().add(inter);
		league.getTeams().add(slovan);
		league.getMatches().add(match);
		league.getMatches().add(match2);
		
		return league;
	}

}
