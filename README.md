﻿# TDT4250 Project Football OSGI

## Members
* Michal Randak
* Tam Duc Ha Vo
* José Ruiz Alarcón
* Simen Sverdrup-Thygeson

## Project Description
Goal of the projec,t is to make web application providing information about football matches. 
Data about matches will be provided by OSGI modules, which can be added or removed at runtime.

## Datasources
So far there are 3 datasources what we will probably use:
* Data about multiple football leagues as REST API: https://www.football-data.org/documentation/quickstart
* Data about few European footbal leagues as SQLITE DB: https://www.kaggle.com/hugomathien/soccer
* Data about world cup in 2018 as JSON FILE: https://raw.githubusercontent.com/lsv/fifa-worldcup-2018/master/data.json

Other datasources might be added, or possibly replace the ones mentioned above.

## Modules
Project consists of multiple modules/projects

#### com.tdt4250.footballproject.model
Model project containing ecore files

#### com.tdt4250.footballproject.model.html
Module containing the model to html transformation logic

#### com.tdt4250.footballproject.model.feature
Project wrapping model project

#### com.tdt4250.footballproject.model.repository
Project serves to wrap features to be available to OSGI framework

#### osgi-workspace/com.tdt4250.footballproject.osgi.api
OSGI module defining interfaces usable in all other OSGI projects

#### osgi-workspace/com.tdt4250.footballproject.osgi.mockmatchservice
OSGI module providing mock implementation of FootballService for integration testing

#### osgi-workspace/com.tdt4250.footballproject.osgi.service.restdatasource
OSGI module implementing FootballService using REST API datasource
Provides gogo console commands:
1. **addService {competitionId}** - creates or modifies service instance in bundle context
2. **removeService {competitionId}** - removes service from bundle context
3. **listServices** - shows list of available instances of RestAPIDatasourceService in bundle context

#### osgi-workspace/com.tdt4250.footballproject.osgi.service.sqlite
OSGI module implementing FootballService using SQLITE datasource

#### osgi-workspace/com.tdt4250.footballproject.osgi.wc18
OSGI module implementing FootballService using JSON file datasource

#### osgi-workspace/com.tdt4250.footballproject.osgi.web
OSGI module implementing servlets to serve data from services as HTML to user




## Development 

### Model project dependency
After each change in Model Project, open **com.tdt4250.footballproject.model.repository/site.xml** in Eclipse and click Build button.

### Adding 3d party dependency
For using any libary, best way is to use maven. Search for the library here: https://mvnrepository.com/repos/central choose version.
Then add new line in **osgi-workspace\cnfcentral.maven** in this format: **{groupId}:{artifactId}:{version}** Now it should be available when adding dependency through build.bnd under Maven Central.

## Operation manual
This section talks about how to use the application, mostly from operation point of view.

OSGI installation is located at **export/osgi/** folder. You can run either **start** or **start.bat** script. After container is started, gogo shell commands can be used to check bundles etc...
For football matches interaction, use **http://localhost:8080/matches/**.
Search by league, start date and end date is possible.
Right now, only two matches are available as proof of concept.

### Scenario One - add datasource module
Lets imagine, we found datasource for world cup 2018 mentioned before. We implemented service getting the the data from this datasource. The bundle is located at **export/additionalJars**.
We can add this code into the running application without stopping it.

Assuming current directory is the same as installation directory:

```
install ../additionalJars/com.tdt4250.footballproject.osgi.wc18.jar
```
The install command shows bundle id for the new module. Running **lb** commands we can see all bundles and their status. Right now, new module should be only in Installed state. 
Therefore run command to start that bundle (replace 55 with correct id, if neccessary).

```
start 55
```
Now there should be league **World Cup - 2018** available to choose. 

### Scenario Two - stop running module
Since now we have the real data, we can stop the Mock bundle serving just dummy data. 

```
lb
```
As mentioned above, this command will show all managed bundles. Find ID for bundle **com.tdt4250.footballproject.osgi.mockmatchservice** and stop it by following command (again, replace 50 if neccessary).

```
stop 50
```

Now, there should not be a Mock League available.

### Scenario Three - adding additional datasources
The same way as scenario one, we can module for handling data from sql database source.

```
install ../additionalJars/com.tdt4250.footballproject.osgi.service.sqlite.jar
install ../additionalJars/com.tdt4250.footballproject.osgi.service.restdatasource.jar
start 56
start 57
```

Now new leagues should be available, serving the data from database. However none of it come from the Rest datasource module.

### Scenario Four - new league in Rest datasource
Both WC18 and SQLITE modules had fixed number of leagues available right after activated. For the REST datasource module, it is up to operations which leagues will be available. 
Currently the REST module uses public REST API for getting football data using free plan. The free plan contains limited number of leagues. Let's imagine, we change the plan and can use more of them. Without changing a line of code and replacing jar, we can tell the module to provide the newly available data.
Run:

```
restdataconsole:addService 2019
restdataconsole:addService 2003
```

This adds Italian Serie A and Netherland Eredivisie to supported leagues. Ids of the leagues can be found in the API documentation http://api.football-data.org/v2/competitions

### Scenario Five - remove league in Rest datasource
The similar way as adding, we can remove league from supported leagues. Let's imagine, one of leagues we supported is no longer available for free from the used API. Therefore we dont want to support it, because it results in errors.

```
restdataconsole:removeService 2019
```

### Scenario Six - changing HTML generation
Let's imagine quite common situation. Our pages doesn't look too good anymore and we want to change the way they're generated without stopping the application. Again, not problem with OSGI. We can make changes in 
**com.tdt4250.footballproject.model.html** project, then generate the plugin jar using 
**com.tdt4250.footballproject.model.repository** and then simply replace old jar. For example purpose, modified jar is 
already prepared in **aditionalJars** folder.

Copy jar **export/additionalJars/com.tdt4250.footballproject.model.html-1.0.0.201911301447.jar** 
to **export/osgi/jar/com.tdt4250.footballproject.model.html-1.0.0.201911301447.jar** to replace the old one.

Then find its bundle id call for update:

```
update 20
```

Now check **http://localhost:8080/matches** to see the change. Title should be coloured green by now.
