/**
 */
package com.tdt4250.footballproject.model.football.impl;

import com.tdt4250.footballproject.model.football.FootballPackage;
import com.tdt4250.footballproject.model.football.League;
import com.tdt4250.footballproject.model.football.Match;
import com.tdt4250.footballproject.model.football.Team;

import java.util.Date;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Match</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.tdt4250.footballproject.model.football.impl.MatchImpl#getHomeTeam <em>Home Team</em>}</li>
 *   <li>{@link com.tdt4250.footballproject.model.football.impl.MatchImpl#getAwayTeam <em>Away Team</em>}</li>
 *   <li>{@link com.tdt4250.footballproject.model.football.impl.MatchImpl#getHomeTeamScore <em>Home Team Score</em>}</li>
 *   <li>{@link com.tdt4250.footballproject.model.football.impl.MatchImpl#getAwayTeamScore <em>Away Team Score</em>}</li>
 *   <li>{@link com.tdt4250.footballproject.model.football.impl.MatchImpl#getWinner <em>Winner</em>}</li>
 *   <li>{@link com.tdt4250.footballproject.model.football.impl.MatchImpl#getDate <em>Date</em>}</li>
 *   <li>{@link com.tdt4250.footballproject.model.football.impl.MatchImpl#getLeague <em>League</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MatchImpl extends MinimalEObjectImpl.Container implements Match {
	/**
	 * The cached value of the '{@link #getHomeTeam() <em>Home Team</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHomeTeam()
	 * @generated
	 * @ordered
	 */
	protected Team homeTeam;

	/**
	 * The cached value of the '{@link #getAwayTeam() <em>Away Team</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAwayTeam()
	 * @generated
	 * @ordered
	 */
	protected Team awayTeam;

	/**
	 * The default value of the '{@link #getHomeTeamScore() <em>Home Team Score</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHomeTeamScore()
	 * @generated
	 * @ordered
	 */
	protected static final int HOME_TEAM_SCORE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getHomeTeamScore() <em>Home Team Score</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHomeTeamScore()
	 * @generated
	 * @ordered
	 */
	protected int homeTeamScore = HOME_TEAM_SCORE_EDEFAULT;

	/**
	 * The default value of the '{@link #getAwayTeamScore() <em>Away Team Score</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAwayTeamScore()
	 * @generated
	 * @ordered
	 */
	protected static final int AWAY_TEAM_SCORE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getAwayTeamScore() <em>Away Team Score</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAwayTeamScore()
	 * @generated
	 * @ordered
	 */
	protected int awayTeamScore = AWAY_TEAM_SCORE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getWinner() <em>Winner</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWinner()
	 * @generated
	 * @ordered
	 */
	protected Team winner;

	/**
	 * The default value of the '{@link #getDate() <em>Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDate()
	 * @generated
	 * @ordered
	 */
	protected static final Date DATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDate() <em>Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDate()
	 * @generated
	 * @ordered
	 */
	protected Date date = DATE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MatchImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FootballPackage.Literals.MATCH;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Team getHomeTeam() {
		if (homeTeam != null && homeTeam.eIsProxy()) {
			InternalEObject oldHomeTeam = (InternalEObject) homeTeam;
			homeTeam = (Team) eResolveProxy(oldHomeTeam);
			if (homeTeam != oldHomeTeam) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, FootballPackage.MATCH__HOME_TEAM,
							oldHomeTeam, homeTeam));
			}
		}
		return homeTeam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Team basicGetHomeTeam() {
		return homeTeam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setHomeTeam(Team newHomeTeam) {
		Team oldHomeTeam = homeTeam;
		homeTeam = newHomeTeam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FootballPackage.MATCH__HOME_TEAM, oldHomeTeam,
					homeTeam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Team getAwayTeam() {
		if (awayTeam != null && awayTeam.eIsProxy()) {
			InternalEObject oldAwayTeam = (InternalEObject) awayTeam;
			awayTeam = (Team) eResolveProxy(oldAwayTeam);
			if (awayTeam != oldAwayTeam) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, FootballPackage.MATCH__AWAY_TEAM,
							oldAwayTeam, awayTeam));
			}
		}
		return awayTeam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Team basicGetAwayTeam() {
		return awayTeam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAwayTeam(Team newAwayTeam) {
		Team oldAwayTeam = awayTeam;
		awayTeam = newAwayTeam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FootballPackage.MATCH__AWAY_TEAM, oldAwayTeam,
					awayTeam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getHomeTeamScore() {
		return homeTeamScore;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setHomeTeamScore(int newHomeTeamScore) {
		int oldHomeTeamScore = homeTeamScore;
		homeTeamScore = newHomeTeamScore;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FootballPackage.MATCH__HOME_TEAM_SCORE,
					oldHomeTeamScore, homeTeamScore));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getAwayTeamScore() {
		return awayTeamScore;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAwayTeamScore(int newAwayTeamScore) {
		int oldAwayTeamScore = awayTeamScore;
		awayTeamScore = newAwayTeamScore;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FootballPackage.MATCH__AWAY_TEAM_SCORE,
					oldAwayTeamScore, awayTeamScore));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Team getWinner() {
		if (winner != null && winner.eIsProxy()) {
			InternalEObject oldWinner = (InternalEObject) winner;
			winner = (Team) eResolveProxy(oldWinner);
			if (winner != oldWinner) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, FootballPackage.MATCH__WINNER, oldWinner,
							winner));
			}
		}
		return winner;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Team basicGetWinner() {
		return winner;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setWinner(Team newWinner) {
		Team oldWinner = winner;
		winner = newWinner;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FootballPackage.MATCH__WINNER, oldWinner, winner));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Date getDate() {
		return date;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDate(Date newDate) {
		Date oldDate = date;
		date = newDate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FootballPackage.MATCH__DATE, oldDate, date));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public League getLeague() {
		if (eContainerFeatureID() != FootballPackage.MATCH__LEAGUE)
			return null;
		return (League) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLeague(League newLeague, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newLeague, FootballPackage.MATCH__LEAGUE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLeague(League newLeague) {
		if (newLeague != eInternalContainer()
				|| (eContainerFeatureID() != FootballPackage.MATCH__LEAGUE && newLeague != null)) {
			if (EcoreUtil.isAncestor(this, newLeague))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newLeague != null)
				msgs = ((InternalEObject) newLeague).eInverseAdd(this, FootballPackage.LEAGUE__MATCHES, League.class,
						msgs);
			msgs = basicSetLeague(newLeague, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FootballPackage.MATCH__LEAGUE, newLeague, newLeague));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case FootballPackage.MATCH__LEAGUE:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetLeague((League) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case FootballPackage.MATCH__LEAGUE:
			return basicSetLeague(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case FootballPackage.MATCH__LEAGUE:
			return eInternalContainer().eInverseRemove(this, FootballPackage.LEAGUE__MATCHES, League.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case FootballPackage.MATCH__HOME_TEAM:
			if (resolve)
				return getHomeTeam();
			return basicGetHomeTeam();
		case FootballPackage.MATCH__AWAY_TEAM:
			if (resolve)
				return getAwayTeam();
			return basicGetAwayTeam();
		case FootballPackage.MATCH__HOME_TEAM_SCORE:
			return getHomeTeamScore();
		case FootballPackage.MATCH__AWAY_TEAM_SCORE:
			return getAwayTeamScore();
		case FootballPackage.MATCH__WINNER:
			if (resolve)
				return getWinner();
			return basicGetWinner();
		case FootballPackage.MATCH__DATE:
			return getDate();
		case FootballPackage.MATCH__LEAGUE:
			return getLeague();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case FootballPackage.MATCH__HOME_TEAM:
			setHomeTeam((Team) newValue);
			return;
		case FootballPackage.MATCH__AWAY_TEAM:
			setAwayTeam((Team) newValue);
			return;
		case FootballPackage.MATCH__HOME_TEAM_SCORE:
			setHomeTeamScore((Integer) newValue);
			return;
		case FootballPackage.MATCH__AWAY_TEAM_SCORE:
			setAwayTeamScore((Integer) newValue);
			return;
		case FootballPackage.MATCH__WINNER:
			setWinner((Team) newValue);
			return;
		case FootballPackage.MATCH__DATE:
			setDate((Date) newValue);
			return;
		case FootballPackage.MATCH__LEAGUE:
			setLeague((League) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case FootballPackage.MATCH__HOME_TEAM:
			setHomeTeam((Team) null);
			return;
		case FootballPackage.MATCH__AWAY_TEAM:
			setAwayTeam((Team) null);
			return;
		case FootballPackage.MATCH__HOME_TEAM_SCORE:
			setHomeTeamScore(HOME_TEAM_SCORE_EDEFAULT);
			return;
		case FootballPackage.MATCH__AWAY_TEAM_SCORE:
			setAwayTeamScore(AWAY_TEAM_SCORE_EDEFAULT);
			return;
		case FootballPackage.MATCH__WINNER:
			setWinner((Team) null);
			return;
		case FootballPackage.MATCH__DATE:
			setDate(DATE_EDEFAULT);
			return;
		case FootballPackage.MATCH__LEAGUE:
			setLeague((League) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case FootballPackage.MATCH__HOME_TEAM:
			return homeTeam != null;
		case FootballPackage.MATCH__AWAY_TEAM:
			return awayTeam != null;
		case FootballPackage.MATCH__HOME_TEAM_SCORE:
			return homeTeamScore != HOME_TEAM_SCORE_EDEFAULT;
		case FootballPackage.MATCH__AWAY_TEAM_SCORE:
			return awayTeamScore != AWAY_TEAM_SCORE_EDEFAULT;
		case FootballPackage.MATCH__WINNER:
			return winner != null;
		case FootballPackage.MATCH__DATE:
			return DATE_EDEFAULT == null ? date != null : !DATE_EDEFAULT.equals(date);
		case FootballPackage.MATCH__LEAGUE:
			return getLeague() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (homeTeamScore: ");
		result.append(homeTeamScore);
		result.append(", awayTeamScore: ");
		result.append(awayTeamScore);
		result.append(", date: ");
		result.append(date);
		result.append(')');
		return result.toString();
	}

} //MatchImpl
