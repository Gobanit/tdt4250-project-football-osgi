/**
 */
package com.tdt4250.footballproject.model.football;

import java.util.Date;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Match</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.tdt4250.footballproject.model.football.Match#getHomeTeam <em>Home Team</em>}</li>
 *   <li>{@link com.tdt4250.footballproject.model.football.Match#getAwayTeam <em>Away Team</em>}</li>
 *   <li>{@link com.tdt4250.footballproject.model.football.Match#getHomeTeamScore <em>Home Team Score</em>}</li>
 *   <li>{@link com.tdt4250.footballproject.model.football.Match#getAwayTeamScore <em>Away Team Score</em>}</li>
 *   <li>{@link com.tdt4250.footballproject.model.football.Match#getWinner <em>Winner</em>}</li>
 *   <li>{@link com.tdt4250.footballproject.model.football.Match#getDate <em>Date</em>}</li>
 *   <li>{@link com.tdt4250.footballproject.model.football.Match#getLeague <em>League</em>}</li>
 * </ul>
 *
 * @see com.tdt4250.footballproject.model.football.FootballPackage#getMatch()
 * @model
 * @generated
 */
public interface Match extends EObject {
	/**
	 * Returns the value of the '<em><b>Home Team</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Home Team</em>' reference.
	 * @see #setHomeTeam(Team)
	 * @see com.tdt4250.footballproject.model.football.FootballPackage#getMatch_HomeTeam()
	 * @model
	 * @generated
	 */
	Team getHomeTeam();

	/**
	 * Sets the value of the '{@link com.tdt4250.footballproject.model.football.Match#getHomeTeam <em>Home Team</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Home Team</em>' reference.
	 * @see #getHomeTeam()
	 * @generated
	 */
	void setHomeTeam(Team value);

	/**
	 * Returns the value of the '<em><b>Away Team</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Away Team</em>' reference.
	 * @see #setAwayTeam(Team)
	 * @see com.tdt4250.footballproject.model.football.FootballPackage#getMatch_AwayTeam()
	 * @model
	 * @generated
	 */
	Team getAwayTeam();

	/**
	 * Sets the value of the '{@link com.tdt4250.footballproject.model.football.Match#getAwayTeam <em>Away Team</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Away Team</em>' reference.
	 * @see #getAwayTeam()
	 * @generated
	 */
	void setAwayTeam(Team value);

	/**
	 * Returns the value of the '<em><b>Home Team Score</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Home Team Score</em>' attribute.
	 * @see #setHomeTeamScore(int)
	 * @see com.tdt4250.footballproject.model.football.FootballPackage#getMatch_HomeTeamScore()
	 * @model
	 * @generated
	 */
	int getHomeTeamScore();

	/**
	 * Sets the value of the '{@link com.tdt4250.footballproject.model.football.Match#getHomeTeamScore <em>Home Team Score</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Home Team Score</em>' attribute.
	 * @see #getHomeTeamScore()
	 * @generated
	 */
	void setHomeTeamScore(int value);

	/**
	 * Returns the value of the '<em><b>Away Team Score</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Away Team Score</em>' attribute.
	 * @see #setAwayTeamScore(int)
	 * @see com.tdt4250.footballproject.model.football.FootballPackage#getMatch_AwayTeamScore()
	 * @model
	 * @generated
	 */
	int getAwayTeamScore();

	/**
	 * Sets the value of the '{@link com.tdt4250.footballproject.model.football.Match#getAwayTeamScore <em>Away Team Score</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Away Team Score</em>' attribute.
	 * @see #getAwayTeamScore()
	 * @generated
	 */
	void setAwayTeamScore(int value);

	/**
	 * Returns the value of the '<em><b>Winner</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Winner</em>' reference.
	 * @see #setWinner(Team)
	 * @see com.tdt4250.footballproject.model.football.FootballPackage#getMatch_Winner()
	 * @model
	 * @generated
	 */
	Team getWinner();

	/**
	 * Sets the value of the '{@link com.tdt4250.footballproject.model.football.Match#getWinner <em>Winner</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Winner</em>' reference.
	 * @see #getWinner()
	 * @generated
	 */
	void setWinner(Team value);

	/**
	 * Returns the value of the '<em><b>Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Date</em>' attribute.
	 * @see #setDate(Date)
	 * @see com.tdt4250.footballproject.model.football.FootballPackage#getMatch_Date()
	 * @model
	 * @generated
	 */
	Date getDate();

	/**
	 * Sets the value of the '{@link com.tdt4250.footballproject.model.football.Match#getDate <em>Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Date</em>' attribute.
	 * @see #getDate()
	 * @generated
	 */
	void setDate(Date value);

	/**
	 * Returns the value of the '<em><b>League</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link com.tdt4250.footballproject.model.football.League#getMatches <em>Matches</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>League</em>' container reference.
	 * @see #setLeague(League)
	 * @see com.tdt4250.footballproject.model.football.FootballPackage#getMatch_League()
	 * @see com.tdt4250.footballproject.model.football.League#getMatches
	 * @model opposite="matches" transient="false"
	 * @generated
	 */
	League getLeague();

	/**
	 * Sets the value of the '{@link com.tdt4250.footballproject.model.football.Match#getLeague <em>League</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>League</em>' container reference.
	 * @see #getLeague()
	 * @generated
	 */
	void setLeague(League value);

} // Match
