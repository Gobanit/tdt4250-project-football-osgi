/**
 */
package com.tdt4250.footballproject.model.football;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Team</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.tdt4250.footballproject.model.football.Team#getPlayers <em>Players</em>}</li>
 *   <li>{@link com.tdt4250.footballproject.model.football.Team#getName <em>Name</em>}</li>
 *   <li>{@link com.tdt4250.footballproject.model.football.Team#getCoach <em>Coach</em>}</li>
 *   <li>{@link com.tdt4250.footballproject.model.football.Team#getPoints <em>Points</em>}</li>
 * </ul>
 *
 * @see com.tdt4250.footballproject.model.football.FootballPackage#getTeam()
 * @model
 * @generated
 */
public interface Team extends EObject {
	/**
	 * Returns the value of the '<em><b>Players</b></em>' containment reference list.
	 * The list contents are of type {@link com.tdt4250.footballproject.model.football.Player}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Players</em>' containment reference list.
	 * @see com.tdt4250.footballproject.model.football.FootballPackage#getTeam_Players()
	 * @model containment="true"
	 * @generated
	 */
	EList<Player> getPlayers();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see com.tdt4250.footballproject.model.football.FootballPackage#getTeam_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link com.tdt4250.footballproject.model.football.Team#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Coach</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Coach</em>' attribute.
	 * @see #setCoach(String)
	 * @see com.tdt4250.footballproject.model.football.FootballPackage#getTeam_Coach()
	 * @model
	 * @generated
	 */
	String getCoach();

	/**
	 * Sets the value of the '{@link com.tdt4250.footballproject.model.football.Team#getCoach <em>Coach</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Coach</em>' attribute.
	 * @see #getCoach()
	 * @generated
	 */
	void setCoach(String value);

	/**
	 * Returns the value of the '<em><b>Points</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Points</em>' attribute.
	 * @see #setPoints(int)
	 * @see com.tdt4250.footballproject.model.football.FootballPackage#getTeam_Points()
	 * @model
	 * @generated
	 */
	int getPoints();

	/**
	 * Sets the value of the '{@link com.tdt4250.footballproject.model.football.Team#getPoints <em>Points</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Points</em>' attribute.
	 * @see #getPoints()
	 * @generated
	 */
	void setPoints(int value);

} // Team
