/**
 */
package com.tdt4250.footballproject.model.football;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>League</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.tdt4250.footballproject.model.football.League#getTeams <em>Teams</em>}</li>
 *   <li>{@link com.tdt4250.footballproject.model.football.League#getName <em>Name</em>}</li>
 *   <li>{@link com.tdt4250.footballproject.model.football.League#getCountry <em>Country</em>}</li>
 *   <li>{@link com.tdt4250.footballproject.model.football.League#getMatches <em>Matches</em>}</li>
 *   <li>{@link com.tdt4250.footballproject.model.football.League#getIconUrl <em>Icon Url</em>}</li>
 *   <li>{@link com.tdt4250.footballproject.model.football.League#getCode <em>Code</em>}</li>
 * </ul>
 *
 * @see com.tdt4250.footballproject.model.football.FootballPackage#getLeague()
 * @model
 * @generated
 */
public interface League extends EObject {
	/**
	 * Returns the value of the '<em><b>Teams</b></em>' containment reference list.
	 * The list contents are of type {@link com.tdt4250.footballproject.model.football.Team}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Teams</em>' containment reference list.
	 * @see com.tdt4250.footballproject.model.football.FootballPackage#getLeague_Teams()
	 * @model containment="true"
	 * @generated
	 */
	EList<Team> getTeams();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see com.tdt4250.footballproject.model.football.FootballPackage#getLeague_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link com.tdt4250.footballproject.model.football.League#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Country</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Country</em>' attribute.
	 * @see #setCountry(String)
	 * @see com.tdt4250.footballproject.model.football.FootballPackage#getLeague_Country()
	 * @model
	 * @generated
	 */
	String getCountry();

	/**
	 * Sets the value of the '{@link com.tdt4250.footballproject.model.football.League#getCountry <em>Country</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Country</em>' attribute.
	 * @see #getCountry()
	 * @generated
	 */
	void setCountry(String value);

	/**
	 * Returns the value of the '<em><b>Matches</b></em>' containment reference list.
	 * The list contents are of type {@link com.tdt4250.footballproject.model.football.Match}.
	 * It is bidirectional and its opposite is '{@link com.tdt4250.footballproject.model.football.Match#getLeague <em>League</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Matches</em>' containment reference list.
	 * @see com.tdt4250.footballproject.model.football.FootballPackage#getLeague_Matches()
	 * @see com.tdt4250.footballproject.model.football.Match#getLeague
	 * @model opposite="league" containment="true"
	 * @generated
	 */
	EList<Match> getMatches();

	/**
	 * Returns the value of the '<em><b>Icon Url</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Icon Url</em>' attribute.
	 * @see #setIconUrl(String)
	 * @see com.tdt4250.footballproject.model.football.FootballPackage#getLeague_IconUrl()
	 * @model
	 * @generated
	 */
	String getIconUrl();

	/**
	 * Sets the value of the '{@link com.tdt4250.footballproject.model.football.League#getIconUrl <em>Icon Url</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Icon Url</em>' attribute.
	 * @see #getIconUrl()
	 * @generated
	 */
	void setIconUrl(String value);

	/**
	 * Returns the value of the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Code</em>' attribute.
	 * @see #setCode(String)
	 * @see com.tdt4250.footballproject.model.football.FootballPackage#getLeague_Code()
	 * @model
	 * @generated
	 */
	String getCode();

	/**
	 * Sets the value of the '{@link com.tdt4250.footballproject.model.football.League#getCode <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Code</em>' attribute.
	 * @see #getCode()
	 * @generated
	 */
	void setCode(String value);

} // League
