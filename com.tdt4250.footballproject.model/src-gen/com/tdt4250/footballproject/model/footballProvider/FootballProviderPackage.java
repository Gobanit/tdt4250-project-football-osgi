/**
 */
package com.tdt4250.footballproject.model.footballProvider;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see com.tdt4250.footballproject.model.footballProvider.FootballProviderFactory
 * @model kind="package"
 * @generated
 */
public interface FootballProviderPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "footballProvider";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.example.org/footballprovider";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "fprov";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	FootballProviderPackage eINSTANCE = com.tdt4250.footballproject.model.footballProvider.impl.FootballProviderPackageImpl.init();

	/**
	 * The meta object id for the '{@link com.tdt4250.footballproject.model.footballProvider.impl.LeagueChooserImpl <em>League Chooser</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.tdt4250.footballproject.model.footballProvider.impl.LeagueChooserImpl
	 * @see com.tdt4250.footballproject.model.footballProvider.impl.FootballProviderPackageImpl#getLeagueChooser()
	 * @generated
	 */
	int LEAGUE_CHOOSER = 0;

	/**
	 * The feature id for the '<em><b>Leagues</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAGUE_CHOOSER__LEAGUES = 0;

	/**
	 * The number of structural features of the '<em>League Chooser</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAGUE_CHOOSER_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>League Chooser</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAGUE_CHOOSER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.tdt4250.footballproject.model.footballProvider.impl.MatchesListResultImpl <em>Matches List Result</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.tdt4250.footballproject.model.footballProvider.impl.MatchesListResultImpl
	 * @see com.tdt4250.footballproject.model.footballProvider.impl.FootballProviderPackageImpl#getMatchesListResult()
	 * @generated
	 */
	int MATCHES_LIST_RESULT = 1;

	/**
	 * The feature id for the '<em><b>Start Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCHES_LIST_RESULT__START_DATE = 0;

	/**
	 * The feature id for the '<em><b>End Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCHES_LIST_RESULT__END_DATE = 1;

	/**
	 * The feature id for the '<em><b>Leagues</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCHES_LIST_RESULT__LEAGUES = 2;

	/**
	 * The feature id for the '<em><b>Filtered Matches</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCHES_LIST_RESULT__FILTERED_MATCHES = 3;

	/**
	 * The number of structural features of the '<em>Matches List Result</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCHES_LIST_RESULT_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Matches List Result</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCHES_LIST_RESULT_OPERATION_COUNT = 0;

	/**
	 * Returns the meta object for class '{@link com.tdt4250.footballproject.model.footballProvider.LeagueChooser <em>League Chooser</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>League Chooser</em>'.
	 * @see com.tdt4250.footballproject.model.footballProvider.LeagueChooser
	 * @generated
	 */
	EClass getLeagueChooser();

	/**
	 * Returns the meta object for the containment reference list '{@link com.tdt4250.footballproject.model.footballProvider.LeagueChooser#getLeagues <em>Leagues</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Leagues</em>'.
	 * @see com.tdt4250.footballproject.model.footballProvider.LeagueChooser#getLeagues()
	 * @see #getLeagueChooser()
	 * @generated
	 */
	EReference getLeagueChooser_Leagues();

	/**
	 * Returns the meta object for class '{@link com.tdt4250.footballproject.model.footballProvider.MatchesListResult <em>Matches List Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Matches List Result</em>'.
	 * @see com.tdt4250.footballproject.model.footballProvider.MatchesListResult
	 * @generated
	 */
	EClass getMatchesListResult();

	/**
	 * Returns the meta object for the attribute '{@link com.tdt4250.footballproject.model.footballProvider.MatchesListResult#getStartDate <em>Start Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Date</em>'.
	 * @see com.tdt4250.footballproject.model.footballProvider.MatchesListResult#getStartDate()
	 * @see #getMatchesListResult()
	 * @generated
	 */
	EAttribute getMatchesListResult_StartDate();

	/**
	 * Returns the meta object for the attribute '{@link com.tdt4250.footballproject.model.footballProvider.MatchesListResult#getEndDate <em>End Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Date</em>'.
	 * @see com.tdt4250.footballproject.model.footballProvider.MatchesListResult#getEndDate()
	 * @see #getMatchesListResult()
	 * @generated
	 */
	EAttribute getMatchesListResult_EndDate();

	/**
	 * Returns the meta object for the containment reference list '{@link com.tdt4250.footballproject.model.footballProvider.MatchesListResult#getLeagues <em>Leagues</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Leagues</em>'.
	 * @see com.tdt4250.footballproject.model.footballProvider.MatchesListResult#getLeagues()
	 * @see #getMatchesListResult()
	 * @generated
	 */
	EReference getMatchesListResult_Leagues();

	/**
	 * Returns the meta object for the reference list '{@link com.tdt4250.footballproject.model.footballProvider.MatchesListResult#getFilteredMatches <em>Filtered Matches</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Filtered Matches</em>'.
	 * @see com.tdt4250.footballproject.model.footballProvider.MatchesListResult#getFilteredMatches()
	 * @see #getMatchesListResult()
	 * @generated
	 */
	EReference getMatchesListResult_FilteredMatches();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	FootballProviderFactory getFootballProviderFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link com.tdt4250.footballproject.model.footballProvider.impl.LeagueChooserImpl <em>League Chooser</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.tdt4250.footballproject.model.footballProvider.impl.LeagueChooserImpl
		 * @see com.tdt4250.footballproject.model.footballProvider.impl.FootballProviderPackageImpl#getLeagueChooser()
		 * @generated
		 */
		EClass LEAGUE_CHOOSER = eINSTANCE.getLeagueChooser();

		/**
		 * The meta object literal for the '<em><b>Leagues</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LEAGUE_CHOOSER__LEAGUES = eINSTANCE.getLeagueChooser_Leagues();

		/**
		 * The meta object literal for the '{@link com.tdt4250.footballproject.model.footballProvider.impl.MatchesListResultImpl <em>Matches List Result</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.tdt4250.footballproject.model.footballProvider.impl.MatchesListResultImpl
		 * @see com.tdt4250.footballproject.model.footballProvider.impl.FootballProviderPackageImpl#getMatchesListResult()
		 * @generated
		 */
		EClass MATCHES_LIST_RESULT = eINSTANCE.getMatchesListResult();

		/**
		 * The meta object literal for the '<em><b>Start Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATCHES_LIST_RESULT__START_DATE = eINSTANCE.getMatchesListResult_StartDate();

		/**
		 * The meta object literal for the '<em><b>End Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATCHES_LIST_RESULT__END_DATE = eINSTANCE.getMatchesListResult_EndDate();

		/**
		 * The meta object literal for the '<em><b>Leagues</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATCHES_LIST_RESULT__LEAGUES = eINSTANCE.getMatchesListResult_Leagues();

		/**
		 * The meta object literal for the '<em><b>Filtered Matches</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATCHES_LIST_RESULT__FILTERED_MATCHES = eINSTANCE.getMatchesListResult_FilteredMatches();

	}

} //FootballProviderPackage
