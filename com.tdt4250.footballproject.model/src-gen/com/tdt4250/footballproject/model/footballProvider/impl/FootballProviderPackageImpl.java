/**
 */
package com.tdt4250.footballproject.model.footballProvider.impl;

import com.tdt4250.footballproject.model.football.FootballPackage;

import com.tdt4250.footballproject.model.footballProvider.FootballProviderFactory;
import com.tdt4250.footballproject.model.footballProvider.FootballProviderPackage;
import com.tdt4250.footballproject.model.footballProvider.LeagueChooser;
import com.tdt4250.footballproject.model.footballProvider.MatchesListResult;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FootballProviderPackageImpl extends EPackageImpl implements FootballProviderPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass leagueChooserEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass matchesListResultEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see com.tdt4250.footballproject.model.footballProvider.FootballProviderPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private FootballProviderPackageImpl() {
		super(eNS_URI, FootballProviderFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link FootballProviderPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static FootballProviderPackage init() {
		if (isInited) return (FootballProviderPackage)EPackage.Registry.INSTANCE.getEPackage(FootballProviderPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredFootballProviderPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		FootballProviderPackageImpl theFootballProviderPackage = registeredFootballProviderPackage instanceof FootballProviderPackageImpl ? (FootballProviderPackageImpl)registeredFootballProviderPackage : new FootballProviderPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		FootballPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theFootballProviderPackage.createPackageContents();

		// Initialize created meta-data
		theFootballProviderPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theFootballProviderPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(FootballProviderPackage.eNS_URI, theFootballProviderPackage);
		return theFootballProviderPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getLeagueChooser() {
		return leagueChooserEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getLeagueChooser_Leagues() {
		return (EReference)leagueChooserEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getMatchesListResult() {
		return matchesListResultEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getMatchesListResult_StartDate() {
		return (EAttribute)matchesListResultEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getMatchesListResult_EndDate() {
		return (EAttribute)matchesListResultEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMatchesListResult_Leagues() {
		return (EReference)matchesListResultEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMatchesListResult_FilteredMatches() {
		return (EReference)matchesListResultEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FootballProviderFactory getFootballProviderFactory() {
		return (FootballProviderFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		leagueChooserEClass = createEClass(LEAGUE_CHOOSER);
		createEReference(leagueChooserEClass, LEAGUE_CHOOSER__LEAGUES);

		matchesListResultEClass = createEClass(MATCHES_LIST_RESULT);
		createEAttribute(matchesListResultEClass, MATCHES_LIST_RESULT__START_DATE);
		createEAttribute(matchesListResultEClass, MATCHES_LIST_RESULT__END_DATE);
		createEReference(matchesListResultEClass, MATCHES_LIST_RESULT__LEAGUES);
		createEReference(matchesListResultEClass, MATCHES_LIST_RESULT__FILTERED_MATCHES);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		FootballPackage theFootballPackage = (FootballPackage)EPackage.Registry.INSTANCE.getEPackage(FootballPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(leagueChooserEClass, LeagueChooser.class, "LeagueChooser", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLeagueChooser_Leagues(), theFootballPackage.getLeague(), null, "leagues", null, 0, -1, LeagueChooser.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(matchesListResultEClass, MatchesListResult.class, "MatchesListResult", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMatchesListResult_StartDate(), ecorePackage.getEDate(), "startDate", null, 0, 1, MatchesListResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMatchesListResult_EndDate(), ecorePackage.getEDate(), "endDate", null, 0, 1, MatchesListResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMatchesListResult_Leagues(), theFootballPackage.getLeague(), null, "leagues", null, 0, -1, MatchesListResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMatchesListResult_FilteredMatches(), theFootballPackage.getMatch(), null, "filteredMatches", null, 0, -1, MatchesListResult.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //FootballProviderPackageImpl
