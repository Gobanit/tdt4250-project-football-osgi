/**
 */
package com.tdt4250.footballproject.model.footballProvider.impl;

import com.tdt4250.footballproject.model.football.League;

import com.tdt4250.footballproject.model.football.Match;
import com.tdt4250.footballproject.model.footballProvider.FootballProviderPackage;
import com.tdt4250.footballproject.model.footballProvider.MatchesListResult;

import java.util.Collection;
import java.util.Date;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Matches List Result</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.tdt4250.footballproject.model.footballProvider.impl.MatchesListResultImpl#getStartDate <em>Start Date</em>}</li>
 *   <li>{@link com.tdt4250.footballproject.model.footballProvider.impl.MatchesListResultImpl#getEndDate <em>End Date</em>}</li>
 *   <li>{@link com.tdt4250.footballproject.model.footballProvider.impl.MatchesListResultImpl#getLeagues <em>Leagues</em>}</li>
 *   <li>{@link com.tdt4250.footballproject.model.footballProvider.impl.MatchesListResultImpl#getFilteredMatches <em>Filtered Matches</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MatchesListResultImpl extends MinimalEObjectImpl.Container implements MatchesListResult {
	/**
	 * The default value of the '{@link #getStartDate() <em>Start Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartDate()
	 * @generated
	 * @ordered
	 */
	protected static final Date START_DATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStartDate() <em>Start Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartDate()
	 * @generated
	 * @ordered
	 */
	protected Date startDate = START_DATE_EDEFAULT;

	/**
	 * The default value of the '{@link #getEndDate() <em>End Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndDate()
	 * @generated
	 * @ordered
	 */
	protected static final Date END_DATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEndDate() <em>End Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndDate()
	 * @generated
	 * @ordered
	 */
	protected Date endDate = END_DATE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLeagues() <em>Leagues</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLeagues()
	 * @generated
	 * @ordered
	 */
	protected EList<League> leagues;

	/**
	 * The cached value of the '{@link #getFilteredMatches() <em>Filtered Matches</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilteredMatches()
	 * @generated
	 * @ordered
	 */
	protected EList<Match> filteredMatches;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MatchesListResultImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FootballProviderPackage.Literals.MATCHES_LIST_RESULT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStartDate(Date newStartDate) {
		Date oldStartDate = startDate;
		startDate = newStartDate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FootballProviderPackage.MATCHES_LIST_RESULT__START_DATE, oldStartDate, startDate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setEndDate(Date newEndDate) {
		Date oldEndDate = endDate;
		endDate = newEndDate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FootballProviderPackage.MATCHES_LIST_RESULT__END_DATE, oldEndDate, endDate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<League> getLeagues() {
		if (leagues == null) {
			leagues = new EObjectContainmentEList<League>(League.class, this, FootballProviderPackage.MATCHES_LIST_RESULT__LEAGUES);
		}
		return leagues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Match> getFilteredMatches() {
		if (filteredMatches == null) {
			filteredMatches = new EObjectResolvingEList<Match>(Match.class, this, FootballProviderPackage.MATCHES_LIST_RESULT__FILTERED_MATCHES);
		}
		return filteredMatches;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FootballProviderPackage.MATCHES_LIST_RESULT__LEAGUES:
				return ((InternalEList<?>)getLeagues()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FootballProviderPackage.MATCHES_LIST_RESULT__START_DATE:
				return getStartDate();
			case FootballProviderPackage.MATCHES_LIST_RESULT__END_DATE:
				return getEndDate();
			case FootballProviderPackage.MATCHES_LIST_RESULT__LEAGUES:
				return getLeagues();
			case FootballProviderPackage.MATCHES_LIST_RESULT__FILTERED_MATCHES:
				return getFilteredMatches();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FootballProviderPackage.MATCHES_LIST_RESULT__START_DATE:
				setStartDate((Date)newValue);
				return;
			case FootballProviderPackage.MATCHES_LIST_RESULT__END_DATE:
				setEndDate((Date)newValue);
				return;
			case FootballProviderPackage.MATCHES_LIST_RESULT__LEAGUES:
				getLeagues().clear();
				getLeagues().addAll((Collection<? extends League>)newValue);
				return;
			case FootballProviderPackage.MATCHES_LIST_RESULT__FILTERED_MATCHES:
				getFilteredMatches().clear();
				getFilteredMatches().addAll((Collection<? extends Match>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FootballProviderPackage.MATCHES_LIST_RESULT__START_DATE:
				setStartDate(START_DATE_EDEFAULT);
				return;
			case FootballProviderPackage.MATCHES_LIST_RESULT__END_DATE:
				setEndDate(END_DATE_EDEFAULT);
				return;
			case FootballProviderPackage.MATCHES_LIST_RESULT__LEAGUES:
				getLeagues().clear();
				return;
			case FootballProviderPackage.MATCHES_LIST_RESULT__FILTERED_MATCHES:
				getFilteredMatches().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FootballProviderPackage.MATCHES_LIST_RESULT__START_DATE:
				return START_DATE_EDEFAULT == null ? startDate != null : !START_DATE_EDEFAULT.equals(startDate);
			case FootballProviderPackage.MATCHES_LIST_RESULT__END_DATE:
				return END_DATE_EDEFAULT == null ? endDate != null : !END_DATE_EDEFAULT.equals(endDate);
			case FootballProviderPackage.MATCHES_LIST_RESULT__LEAGUES:
				return leagues != null && !leagues.isEmpty();
			case FootballProviderPackage.MATCHES_LIST_RESULT__FILTERED_MATCHES:
				return filteredMatches != null && !filteredMatches.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (startDate: ");
		result.append(startDate);
		result.append(", endDate: ");
		result.append(endDate);
		result.append(')');
		return result.toString();
	}

} //MatchesListResultImpl
