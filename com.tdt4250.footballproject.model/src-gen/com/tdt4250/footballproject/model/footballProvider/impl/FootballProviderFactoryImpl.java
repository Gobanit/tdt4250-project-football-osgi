/**
 */
package com.tdt4250.footballproject.model.footballProvider.impl;

import com.tdt4250.footballproject.model.footballProvider.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FootballProviderFactoryImpl extends EFactoryImpl implements FootballProviderFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static FootballProviderFactory init() {
		try {
			FootballProviderFactory theFootballProviderFactory = (FootballProviderFactory)EPackage.Registry.INSTANCE.getEFactory(FootballProviderPackage.eNS_URI);
			if (theFootballProviderFactory != null) {
				return theFootballProviderFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new FootballProviderFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FootballProviderFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case FootballProviderPackage.LEAGUE_CHOOSER: return createLeagueChooser();
			case FootballProviderPackage.MATCHES_LIST_RESULT: return createMatchesListResult();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LeagueChooser createLeagueChooser() {
		LeagueChooserImpl leagueChooser = new LeagueChooserImpl();
		return leagueChooser;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MatchesListResult createMatchesListResult() {
		MatchesListResultImpl matchesListResult = new MatchesListResultImpl();
		return matchesListResult;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FootballProviderPackage getFootballProviderPackage() {
		return (FootballProviderPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static FootballProviderPackage getPackage() {
		return FootballProviderPackage.eINSTANCE;
	}

} //FootballProviderFactoryImpl
