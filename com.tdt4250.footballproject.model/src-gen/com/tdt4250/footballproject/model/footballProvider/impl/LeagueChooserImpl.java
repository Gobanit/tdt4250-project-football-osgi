/**
 */
package com.tdt4250.footballproject.model.footballProvider.impl;

import com.tdt4250.footballproject.model.football.League;

import com.tdt4250.footballproject.model.footballProvider.FootballProviderPackage;
import com.tdt4250.footballproject.model.footballProvider.LeagueChooser;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>League Chooser</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.tdt4250.footballproject.model.footballProvider.impl.LeagueChooserImpl#getLeagues <em>Leagues</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LeagueChooserImpl extends MinimalEObjectImpl.Container implements LeagueChooser {
	/**
	 * The cached value of the '{@link #getLeagues() <em>Leagues</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLeagues()
	 * @generated
	 * @ordered
	 */
	protected EList<League> leagues;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LeagueChooserImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FootballProviderPackage.Literals.LEAGUE_CHOOSER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<League> getLeagues() {
		if (leagues == null) {
			leagues = new EObjectContainmentEList<League>(League.class, this, FootballProviderPackage.LEAGUE_CHOOSER__LEAGUES);
		}
		return leagues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FootballProviderPackage.LEAGUE_CHOOSER__LEAGUES:
				return ((InternalEList<?>)getLeagues()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FootballProviderPackage.LEAGUE_CHOOSER__LEAGUES:
				return getLeagues();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FootballProviderPackage.LEAGUE_CHOOSER__LEAGUES:
				getLeagues().clear();
				getLeagues().addAll((Collection<? extends League>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FootballProviderPackage.LEAGUE_CHOOSER__LEAGUES:
				getLeagues().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FootballProviderPackage.LEAGUE_CHOOSER__LEAGUES:
				return leagues != null && !leagues.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //LeagueChooserImpl
