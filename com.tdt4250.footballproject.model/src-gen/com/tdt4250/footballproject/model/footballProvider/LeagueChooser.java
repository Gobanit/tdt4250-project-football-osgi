/**
 */
package com.tdt4250.footballproject.model.footballProvider;

import com.tdt4250.footballproject.model.football.League;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>League Chooser</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.tdt4250.footballproject.model.footballProvider.LeagueChooser#getLeagues <em>Leagues</em>}</li>
 * </ul>
 *
 * @see com.tdt4250.footballproject.model.footballProvider.FootballProviderPackage#getLeagueChooser()
 * @model
 * @generated
 */
public interface LeagueChooser extends EObject {
	/**
	 * Returns the value of the '<em><b>Leagues</b></em>' containment reference list.
	 * The list contents are of type {@link com.tdt4250.footballproject.model.football.League}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Leagues</em>' containment reference list.
	 * @see com.tdt4250.footballproject.model.footballProvider.FootballProviderPackage#getLeagueChooser_Leagues()
	 * @model containment="true"
	 * @generated
	 */
	EList<League> getLeagues();

} // LeagueChooser
