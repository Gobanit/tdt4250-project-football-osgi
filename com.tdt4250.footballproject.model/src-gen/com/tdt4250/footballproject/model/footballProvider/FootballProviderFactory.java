/**
 */
package com.tdt4250.footballproject.model.footballProvider;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see com.tdt4250.footballproject.model.footballProvider.FootballProviderPackage
 * @generated
 */
public interface FootballProviderFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	FootballProviderFactory eINSTANCE = com.tdt4250.footballproject.model.footballProvider.impl.FootballProviderFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>League Chooser</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>League Chooser</em>'.
	 * @generated
	 */
	LeagueChooser createLeagueChooser();

	/**
	 * Returns a new object of class '<em>Matches List Result</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Matches List Result</em>'.
	 * @generated
	 */
	MatchesListResult createMatchesListResult();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	FootballProviderPackage getFootballProviderPackage();

} //FootballProviderFactory
