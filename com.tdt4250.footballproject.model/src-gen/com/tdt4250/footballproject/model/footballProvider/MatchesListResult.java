/**
 */
package com.tdt4250.footballproject.model.footballProvider;

import com.tdt4250.footballproject.model.football.League;

import com.tdt4250.footballproject.model.football.Match;
import java.util.Date;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Matches List Result</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.tdt4250.footballproject.model.footballProvider.MatchesListResult#getStartDate <em>Start Date</em>}</li>
 *   <li>{@link com.tdt4250.footballproject.model.footballProvider.MatchesListResult#getEndDate <em>End Date</em>}</li>
 *   <li>{@link com.tdt4250.footballproject.model.footballProvider.MatchesListResult#getLeagues <em>Leagues</em>}</li>
 *   <li>{@link com.tdt4250.footballproject.model.footballProvider.MatchesListResult#getFilteredMatches <em>Filtered Matches</em>}</li>
 * </ul>
 *
 * @see com.tdt4250.footballproject.model.footballProvider.FootballProviderPackage#getMatchesListResult()
 * @model
 * @generated
 */
public interface MatchesListResult extends EObject {
	/**
	 * Returns the value of the '<em><b>Start Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Date</em>' attribute.
	 * @see #setStartDate(Date)
	 * @see com.tdt4250.footballproject.model.footballProvider.FootballProviderPackage#getMatchesListResult_StartDate()
	 * @model
	 * @generated
	 */
	Date getStartDate();

	/**
	 * Sets the value of the '{@link com.tdt4250.footballproject.model.footballProvider.MatchesListResult#getStartDate <em>Start Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Date</em>' attribute.
	 * @see #getStartDate()
	 * @generated
	 */
	void setStartDate(Date value);

	/**
	 * Returns the value of the '<em><b>End Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Date</em>' attribute.
	 * @see #setEndDate(Date)
	 * @see com.tdt4250.footballproject.model.footballProvider.FootballProviderPackage#getMatchesListResult_EndDate()
	 * @model
	 * @generated
	 */
	Date getEndDate();

	/**
	 * Sets the value of the '{@link com.tdt4250.footballproject.model.footballProvider.MatchesListResult#getEndDate <em>End Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Date</em>' attribute.
	 * @see #getEndDate()
	 * @generated
	 */
	void setEndDate(Date value);

	/**
	 * Returns the value of the '<em><b>Leagues</b></em>' containment reference list.
	 * The list contents are of type {@link com.tdt4250.footballproject.model.football.League}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Leagues</em>' containment reference list.
	 * @see com.tdt4250.footballproject.model.footballProvider.FootballProviderPackage#getMatchesListResult_Leagues()
	 * @model containment="true"
	 * @generated
	 */
	EList<League> getLeagues();

	/**
	 * Returns the value of the '<em><b>Filtered Matches</b></em>' reference list.
	 * The list contents are of type {@link com.tdt4250.footballproject.model.football.Match}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Filtered Matches</em>' reference list.
	 * @see com.tdt4250.footballproject.model.footballProvider.FootballProviderPackage#getMatchesListResult_FilteredMatches()
	 * @model
	 * @generated
	 */
	EList<Match> getFilteredMatches();

} // MatchesListResult
