package com.tdt4250.footballproject.model.sandbox;

import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

import com.tdt4250.footballproject.model.football.FootballFactory;
import com.tdt4250.footballproject.model.football.League;
import com.tdt4250.footballproject.model.football.Match;
import com.tdt4250.footballproject.model.football.Team;
import com.tdt4250.footballproject.model.football.impl.FootballFactoryImpl;

public class FootbalModelTest {

	public static void main(String[] args) throws IOException {
		FootballFactory fact = FootballFactoryImpl.eINSTANCE;
		League league = fact.createLeague();
		league.setCountry("Slovakia");
		league.setName("Corgoň liga");
		
		Team inter = fact.createTeam();
		inter.setName("Inter Bratislava");
		
		Team slovan = fact.createTeam();
		slovan.setName("Slovan Bratislava");
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm").withZone(ZoneId.systemDefault());
		
		Match match = fact.createMatch();
		match.setHomeTeam(inter);
		match.setAwayTeam(slovan);
		match.setDate(Date.from(Instant.from(dtf.parse("2019-12-14 18:00"))));
		
		league.getTeams().add(inter);
		league.getTeams().add(slovan);
		league.getMatches().add(match);
		
		Resource resource = new XMIResourceImpl(URI.createURI("./resource1.xmi"));
		resource.getContents().add(league);
		
		ResourceSet resourceSet = new ResourceSetImpl();
		resourceSet.getResources().add(resource);
		Map<Object,Object> options = new HashMap<Object,Object>();
		options.put(XMIResource.OPTION_SCHEMA_LOCATION, Boolean.TRUE);
		
		resource.save(options);

	}

}
